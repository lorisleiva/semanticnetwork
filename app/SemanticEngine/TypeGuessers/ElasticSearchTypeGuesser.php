<?php

namespace App\SemanticEngine\TypeGuessers;

use ES;

class ElasticSearchTypeGuesser implements TypeGuesser
{
    const MAX_PERCENT  = .8;
    const MEAN_PERCENT = .5;
    const MIN_PERCENT  = .2;

    const MAX_NBR_CANDIDATES  = 10;
    const MAX_NBR_SUGGESTIONS  = 5;

    const WEIGHT_TYPES = .8;
    const WEIGHT_MEAN = 1;

    const ACCEPTANCE_THRESHOLD = .7;
    const SUGGESTION_THRESHOLD = .55;

    protected $previousTypeGuesser;
    protected $typeSuggestions;

    protected $candidates;
    protected $mean;
    protected $variance;
    protected $min;
    protected $max;

    function __construct(TypeGuesser $previousTypeGuesser = null)
    {
        $this->previousTypeGuesser = $previousTypeGuesser;
    }

    public function guess($query)
    {
        if ($this->previousTypeGuesser) {
            $this->previousTypeGuesser->guess($query);
        }

        $this->typeSuggestions = $query['suggestions'];

        $this->guessTypeAndSuggestions($query);

        if ($query['type'] && empty($query['suggestions'])) {
            $this->guessTypeAndSuggestions($query, true);
        }
    }

    private function guessTypeAndSuggestions($query, $withType = false)
    {
        $this->fetchCandidates($query, $withType);
        $this->computeStatistics();
        $this->computeInitialProbabilities();
        $this->factorTypeSuggestions();
        $this->normalizeCandidates();
        $this->updateQueryWithCandidates($query);
    }

    private function fetchCandidates($query, $withType = false)
    {
        $type = $withType ? $query->get('type') : null;

        $esData = ES::search($query->get('search'), $type)
            ->first(self::MAX_NBR_CANDIDATES);

        if (array_key_exists('_source', $esData)) {
            $esData = [ $esData ];
        }

        $this->candidates = collect([]);
        foreach ($esData as $candidate) {
            $this->candidates->push(collect([
                'title' => $this->getCandidateName($candidate),
                'id' => $candidate['_id'],
                'type' => $candidate['_type'],
                'score' => $candidate['_score'],
                'probability' => .5,
            ]));
        }
    }

    private function getCandidateName($candidate)
    {
        $nameMap = [
            "IpAddress" => "ipv4"
        ];

        if (array_key_exists($candidate['_type'], $nameMap)
            && array_key_exists($nameMap[$candidate['_type']], $candidate['_source'])) 
        {
            return $candidate['_source'][$nameMap[$candidate['_type']]];
        }

        return $candidate['_id'];
    }

    private function computeStatistics()
    {
        $values = $this->candidates->pluck('score')->toArray();
        $this->mean = average($values);
        $this->variance = variance($values);
        $this->min = empty($values) ? 0 : min($values);
        $this->max = empty($values) ? 0 : max($values);
    }

    private function computeInitialProbabilities()
    {
        if ($this->invalidSetOfCandidates()) {
            return;
        }

        // $deltaPercent = $this->variance / 100;
        $deltaPercent = $this->variance / ($this->max - $this->min);
        $topPercent = min(self::MAX_PERCENT, .5 + $deltaPercent);
        $bottomPercent = max(self::MIN_PERCENT, .5 - $deltaPercent);

        $coefBelowMean = (.5 - $bottomPercent) / ($this->mean - $this->min);
        $originBelowMean = .5 - ($coefBelowMean * $this->mean);

        $coefAboveMean = ($topPercent - .5) / ($this->max - $this->mean);
        $originAboveMean = .5 - ($coefAboveMean * $this->mean);

        foreach ($this->candidates as $candidate) {
            $score = $candidate['score'];
            if ($score < $this->mean) {
                $probability = ($coefBelowMean * $score) + $originBelowMean;
            } else {
                $probability = ($coefAboveMean * $score) + $originAboveMean;
            }
            $probability += $probability * ($this->mean / 100) * self::WEIGHT_MEAN;
            $candidate['probability'] = $probability;
        }
    }

    private function factorTypeSuggestions()
    {
        if (empty($this->typeSuggestions)) {
            return;
        }

        $typeMap = [];
        foreach ($this->typeSuggestions as $suggestion) {
            $typeMap[$suggestion['type']] = $suggestion['probability'];
        }

        foreach ($this->candidates as $candidate) {
            if (array_key_exists($candidate['type'], $typeMap)) {
                $coef = $typeMap[$candidate['type']] * self::WEIGHT_TYPES;
                $candidate['probability'] += $candidate['probability'] * $coef;
            }
        }
    }

    private function normalizeCandidates()
    {
        $maxProbability = $this->candidates->pluck('probability')->max();

        if ($maxProbability <= 1) {
            return;
        }

        foreach ($this->candidates as $candidate) {
            $candidate['probability'] /= $maxProbability;
        }
    }

    private function updateQueryWithCandidates($query)
    {
        $this->candidates = $this->candidates->sortBy('probability');
        $this->updateQueryType($query);
        $this->updateQuerySuggestions($query);
    }

    private function updateQueryType($query)
    {
        $bestCandidate = $this->candidates->last();

        if ($query['type'] === $bestCandidate['type']) {
            $this->candidates->pop();
            return;
        }

        if ($query['type']) {
            return;
        }

        if ($bestCandidate['probability'] < self::ACCEPTANCE_THRESHOLD) {
            return;
        }

        $query['type'] = $bestCandidate['type'];
        $this->candidates->pop();
    }

    private function updateQuerySuggestions($query)
    {
        $suggestions = [];
        $bestCandidates = $this->candidates->take(- self::MAX_NBR_SUGGESTIONS);

        foreach ($bestCandidates as $candidate) {
            if ($candidate['probability'] >= self::SUGGESTION_THRESHOLD) {
                $suggestions[] = $candidate->except('score')->toArray();
            }
        }

        $query['suggestions'] = array_reverse($suggestions);
    }

    private function invalidSetOfCandidates()
    {
        return ($this->mean - $this->min) == 0
            || ($this->max - $this->mean) == 0
            || ($this->max - $this->min) == 0;
    }
}