<?php

namespace App\SemanticEngine\TypeGuessers;

class RegexTypeGuesser implements TypeGuesser
{
    const REGEX = [
        "/(\d{1,3}\.){3}\d{1,3}/" => "IpAddress",
        "/\sdoe$/i" => "Person",
        // Show: Elizabeth Montgomery Doe
        // Progressif: /doe/
        // Progressif: /doe$/
        // Progressif: /Doe$/ or /doe$/i
        // Progressif: /\sdoe$/i
    ];

    protected $previousTypeGuesser;

    function __construct(TypeGuesser $previousTypeGuesser = null)
    {
        $this->previousTypeGuesser = $previousTypeGuesser;
    }

    public function guess($query)
    {
        if ($this->previousTypeGuesser) {
            $this->previousTypeGuesser->guess($query);
        }

        if ($query['type']) {
            return;
        }
        
        $this->findMatchingType($query);
    }

    private function findMatchingType($query)
    {
        foreach (self::REGEX as $regex => $type) {
            if (preg_match($regex, $query['search'])) {
                $query['type'] = $type;
                break;
            }
        }
    }
}