<?php

namespace App\SemanticEngine\TypeGuessers;

interface TypeGuesser
{
    public function guess($query);
}