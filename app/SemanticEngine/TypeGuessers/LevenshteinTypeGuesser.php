<?php

namespace App\SemanticEngine\TypeGuessers;

use App\MachineLearning\Classifiers\LevenshteinClassifier;
use App\MachineLearning\Documents\RecordDocument;
use App\MachineLearning\FeatureFactories\FeaturesByAccessors;
use App\MachineLearning\Models\SimpleDBModel;

class LevenshteinTypeGuesser implements TypeGuesser
{
    const ACCEPTANCE_THRESHOLD = .7;
    const SUGGESTION_THRESHOLD = .5;

    protected $previousTypeGuesser;

    function __construct(TypeGuesser $previousTypeGuesser = null)
    {
        $this->previousTypeGuesser = $previousTypeGuesser;
    }

    public function guess($query)
    {
        if ($this->previousTypeGuesser) {
            $this->previousTypeGuesser->guess($query);
        }
        
        $model = new SimpleDBModel('ml_typeguesser');
        $ff = new FeaturesByAccessors(['query', 'type']);
        $cls = new LevenshteinClassifier($model, $ff);
        $result = $cls->classify(new RecordDocument([
            'query' => $query->get('search')
        ]));

        if (! $query->get('type') && $this->acceptanceThresholdReached($result->last())) {
            $query->put('type', $result->keys()->last());
        }

        $this->identifySuggestions($query, $result);
    }

    private function identifySuggestions($query, $result)
    {
        $suggestions = [];

        foreach ($result as $type => $probability) {
            if ($this->suggestionThresholdReached($probability)) {
                $suggestions[] = [
                    'title' => null,
                    'type' => $type,
                    'probability' => $probability
                ];
            }
        }

        $query->put('suggestions', $suggestions);
    }

    private function acceptanceThresholdReached($probability)
    {
        return $probability >= self::ACCEPTANCE_THRESHOLD;
    }

    private function suggestionThresholdReached($probability)
    {
        return $probability >= self::SUGGESTION_THRESHOLD;
    }
}