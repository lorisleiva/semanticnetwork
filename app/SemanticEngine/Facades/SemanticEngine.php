<?php
namespace App\SemanticEngine\Facades;

use Illuminate\Support\Facades\Facade;

class SemanticEngine extends Facade
{
    protected static function getFacadeAccessor() { return 'semanticengine'; }
}