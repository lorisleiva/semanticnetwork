<?php

namespace App\SemanticEngine\Fetchers;

use App\SemanticEngine\Data\RawData;

abstract class Fetcher
{
    protected $query;
    protected $rawData;

    abstract public function fetch();

    function __construct($query)
    {
        $this->query = $query;
        $this->rawData = new RawData;
    }
}