<?php

namespace App\SemanticEngine\Fetchers;

class FallbackFetcher extends Fetcher
{
    public function fetch()
    {
        return $this->rawData;
    }
}