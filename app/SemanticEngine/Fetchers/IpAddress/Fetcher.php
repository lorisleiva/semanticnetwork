<?php

namespace App\SemanticEngine\Fetchers\IpAddress;

use App\SemanticEngine\Fetchers\Support\BroFetcher;

class Fetcher extends BroFetcher
{
    protected $logTypes = [
        ['conn', 'bro.connection']
    ];
}