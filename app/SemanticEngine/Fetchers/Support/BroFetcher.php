<?php

namespace App\SemanticEngine\Fetchers\Support;

use App\SemanticEngine\Fetchers\Fetcher;
use Carbon\Carbon;

abstract class BroFetcher extends Fetcher
{
    protected $dateFromEST = null;
    protected $timeFromEST = null;
    protected $dateToEST = null;
    protected $timeToEST = null;
    protected $sepDate = "-";
    protected $sepTime = ":";
    protected $logsFolder = "/usr/local/var/logs";

    function __construct($query)
    {
        parent::__construct($query);
        if (! is_null($this->query->get('date_from'))) {
            $datetime = $this->parseCarbon('date_from');
            $this->dateFromEST = $datetime[0];
            $this->timeFromEST = $datetime[1];
        }
        if (! is_null($this->query->get('date_to'))) {
            $datetime = $this->parseCarbon('date_to');
            $this->dateToEST = $datetime[0];
            $this->timeToEST = $datetime[1];
        }
    }

    public function fetch()
    {
        if (! property_exists($this, "logTypes")) {
           return $this->rawData;
        }

        foreach ($this->logTypes as $logType) {
            $this->addCurrentBroFile($logType[0], $logType[1]);
            $this->addArchiveBroFiles($logType[0], $logType[1]);
        }

        return $this->rawData;
    }

    public function addCurrentBroFile($logfile, $parserPath)
    {
        $dateTo = $this->query->get('date_to');
        $todayMidnight = Carbon::now('EST')->startOfDay();

        if (is_null($dateTo) || $dateTo->gte($todayMidnight)) {
            $logFile = "$this->logsFolder/current/$logfile.log";
            $this->rawData->addFile($logFile, $parserPath);
        }
    }

    public function addArchiveBroFiles($logfile, $parserPath)
    {
        $archiveFolders = $this->getArchiveFolders();

        foreach ($archiveFolders as $archiveFolder) {
            $archiveFiles = $this->getArchiveFiles($archiveFolder, $logfile);

            foreach ($archiveFiles as $archiveFile) {
                $logFile = "$this->logsFolder/$archiveFolder/$archiveFile";
                $this->rawData->add('file_gz', $logFile, $parserPath);
            }
        }
    }

    private function getArchiveFolders()
    {
        return collect(scandir($this->logsFolder))
            ->filter($this->isValidArchiveFolder());
    }

    private function isValidArchiveFolder()
    {
        $dateFrom = $this->dateFromEST;
        $dateTo = $this->dateToEST;

        return function($dirName) use($dateFrom, $dateTo) {
            if (! preg_match("/^\d{4}(?:-\d{2}){2}$/", $dirName)) {
                return false;
            }

            if (! is_null($dateFrom) && $dirName < $dateFrom) {
                return false;
            }

            if (! is_null($dateTo) && $dirName > $dateTo) {
                return false;
            }

            return true;
        };
    }

    private function getArchiveFiles($folder, $logfile)
    {
        return collect(scandir("$this->logsFolder/$folder"))
            ->filter($this->isValidArchiveFile($folder, $logfile));
    }

    private function isValidArchiveFile($folder, $logfile)
    {
        $timeFrom = $this->dateFromEST === $folder ? $this->timeFromEST : null;
        $timeTo = $this->dateToEST === $folder ? $this->timeToEST : null;

        return function($fileName) use($logfile, $timeFrom, $timeTo) {
            $regexTime = "\d{2}$this->sepTime\d{2}$this->sepTime\d{2}";
            $regexDur = "($regexTime)-($regexTime)";

            if (! preg_match("/^\w+.$regexDur.log.gz$/", $fileName, $times)) {
                return false;
            }

            if (! starts_with($fileName, $logfile)) {
                return false;
            }

            $fileFrom = $times[1];
            $fileTo = $times[2];

            if ($fileTo === "00{$this->sepTime}00{$this->sepTime}00") {
                $fileTo = "24{$this->sepTime}00{$this->sepTime}00";
            }

            if (! is_null($timeFrom) && $timeFrom >= $fileTo) {
                return false;
            }

            if (! is_null($timeTo) && $timeTo <= $fileFrom) {
                return false;
            }

            return true;
        };
    }

    private function parseCarbon($field)
    {
        $carbon = clone $this->query->get($field);
        $carbon->setTimezone('EST');
        return [
            $carbon->format("Y{$this->sepDate}m{$this->sepDate}d"),
            $carbon->format("H{$this->sepTime}i{$this->sepTime}s")
        ];
    }
}