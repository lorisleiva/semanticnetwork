<?php

namespace App\SemanticEngine\Fetchers\Person;

use App\SemanticEngine\Fetchers\Fetcher as BaseFetcher;

class Fetcher extends BaseFetcher
{
    protected $twitterBaseUrl = 'https://twitter.com/';

    public function fetch()
    {
        $type = 'url';
        $parser = 'social.twitter';
        $data = $this->twitterBaseUrl . $this->getUsernameFromQuery();

        $this->rawData->add($type, $data, $parser);

        return $this->rawData;
    }

    private function getUsernameFromQuery()
    {
        $username = preg_replace('/\s+/', '', strtolower($this->query['search']));
        return $username === 'lorisleiva' ? 'ketsio' : $username;
    }
}