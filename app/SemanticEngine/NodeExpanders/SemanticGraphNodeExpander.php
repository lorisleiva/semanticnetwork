<?php

namespace App\SemanticEngine\NodeExpanders;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Data;

class SemanticGraphNodeExpander implements NodeExpander
{
    protected $semData;
    protected $humData;
    protected $node;
    protected $defaultDepth = 2;

    protected $nodesTraveled;
    protected $nextNodes;

    function __construct($semData, $node, $humData = null)
    {
        $this->humData = is_null($humData) ? new HumanData($node) : $humData;
        $this->semData = $semData;
        $this->node = $node;
    }

    public function expand($depth = null)
    {
        // return $this->humData;
        // Show defaultDepth = 1
        // Show defaultDepth = 2

        if ($this->node) {
            $initNodes = collect([$this->node->getUid()]);
            $initDepth = is_null($depth) ? $this->defaultDepth : $depth;
            $this->nodesTraveled = collect([]);
            $this->expendNodesRecursive($initNodes, $initDepth);
        }

        return $this->humData;
    }

    protected function expendNodesRecursive($nodes, $depth)
    {
        if ($depth <= 0) {
            return;
        }

        $this->nextNodes = collect([]);

        foreach ($nodes as $node) {
            $this->humData->addNode($node);
            $this->markNodeAsTraveled($node);
            $this->addForwardTuples($node);
            $this->addBackwardTuples($node);
        }

        $this->expendNodesRecursive($this->nextNodes, $depth - 1);
    }


    protected function markNodeAsTraveled($node)
    {
        if (! $this->nodesTraveled->contains($node)) {
            $this->nodesTraveled->push($node);
        }
    }

    protected function addNextNodes($node)
    {
        if (! $this->nodesTraveled->contains($node) && 
            ! $this->nextNodes->contains($node))
        {
            $this->nextNodes->push($node);
        }
    }

    protected function addForwardTuples($node)
    {
        $forwardTuples = $this->semData->get($node);
        foreach ($forwardTuples as $relation => $values) {
            foreach ($values as $value) {
                if ($this->semData->hasSource($value)) {
                    $this->humData->addNode($value);
                    $this->addNextNodes($value);
                }
                
                $this->humData->addTo($node, $relation, $value);
            }
        }
    }

    protected function addBackwardTuples($node)
    {
        $backwardTuples = $this->semData->getReversed($node);
        foreach ($backwardTuples as $relation => $srcNodes) {
            foreach ($srcNodes as $srcNode) {
                $type = $this->semData->getType($srcNode);
                $this->humData->addNode($srcNode);
                $this->addNextNodes($srcNode);
                $this->humData->addTo($node, strtolower($type) . 'Associated', $srcNode);
                $this->humData->addTo($srcNode, Data::AS_RELATION, $relation);
            }
        }
    }
}