<?php

namespace App\SemanticEngine\NodeExpanders;

use App\ElasticSearch\NodeAdapter;
use App\SemanticEngine\Data\Data;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use ES;

class ElasticSearchNodeExpander implements NodeExpander
{
    protected $humData;
    protected $node;
    protected $defaultDepth = 2;

    protected $nodesTraveled;
    protected $nextNodes;

    function __construct($node, HumanData $humData = null)
    {
        $this->humData = $humData ?: new HumanData($node);
        $this->node = $node;
    }

    public function expand($depth = null)
    {
        if ($this->node) {
            $initNodes = collect([$this->node]);
            $initDepth = $depth ?: $this->defaultDepth;
            $this->nodesTraveled = collect([]);
            $this->expendNodesRecursive($initNodes, $initDepth);
        }
        
        return $this->humData;
    }

    protected function expendNodesRecursive($nodes, $depth)
    {
        if ($depth <= 0) {
            return;
        }

        $this->nextNodes = collect([]);

        foreach ($nodes as $node) {
            $this->humData->addNode($node);
            $this->markNodeAsTraveled($node);

            $rules = $this->getRules($node->getType());

            foreach ($rules as $relation => $rule) {
                $this->parseRule($relation, $rule, $node);
            }
        }

        $this->expendNodesRecursive($this->nextNodes, $depth - 1);
    }

    protected function parseRule($relation, $rule, $node)
    {
        if (is_string($rule)) {
            $rule = [ 'type' => $rule ];
        }

        if (! array_key_exists('type', $rule)) {
            return;
        }

        if (array_key_exists('as', $rule)) {
            $this->parseBackwardRule($relation, $rule, $node);
        } else {
            $this->parseForwardRule($relation, $rule, $node);
        }
    }

    protected function parseForwardRule($relation, $rule, $node)
    {
        if (! $node->has($relation)) {
            return;
        }

        $child = $this->humData->getNode($node->get($relation));

        if (! $child) {
            $esData = ES::find($node->get($relation), $rule['type']);

            if (empty($esData)) {
                return;
            }

            $child = new NodeAdapter($esData);
        }

        $node->remove($relation);
        $this->addChild($node, $relation, $child);
    }

    protected function parseBackwardRule($relation, $rule, $node)
    {
        $backwardQuery = $this->getBackwardSearchQuery($rule, $node);
        if (str_contains($rule['type'], '|')) {
            $es = ES::search($backwardQuery);
        } else {
            $es = ES::search($backwardQuery, $rule['type']);
        }
        $esData = $this->getResultsBasedOnRelationship($es, $rule);

        if (empty($esData)) {
            return;
        }

        $children = NodeAdapter::generateAll($esData);
        foreach ($children as $child) {
            $this->resolveAsRelation($rule['as'], $node, $child);
            $this->addChild($node, $relation, $child);
        }
    }

    protected function getBackwardSearchQuery($rule, $node)
    {
        $uid = $node->getUid();

        $query = array_map(function ($asRelation) use ($uid) {
            return "$asRelation:$uid";
        }, explode('|', $rule['as']));
        $query = implode(' OR ', $query);
        $query = "($query)";


        if (str_contains($rule['type'], '|')) {
            $typeQuery = array_map(function ($typeRelation) {
                return "type:$typeRelation";
            }, explode('|', $rule['type']));
            $typeQuery = implode(' OR ', $typeQuery);
            $query .= "AND ($typeQuery)";
        }

        return $query;
    }

    protected function getResultsBasedOnRelationship($es, $rule)
    {
        if (! array_key_exists('relationship', $rule) || 
            $rule['relationship'] === 'one')
        {
            return $es->first();
        }

        if (is_integer($rule['relationship'])) {
            return $es->first($rule['relationship']);
        }

        return $es->all();
    }

    protected function resolveAsRelation($asField, $node, $child)
    {
        $uid = $node->getUid();
        $asRelation = null;

        foreach (explode('|', $asField) as $relation) {
            if ($child->get($relation) === $uid) {
                $asRelation = $relation;
                break;
            }
        }

        if ($asRelation) {
            $child->add(Data::AS_RELATION, $asRelation);
        }
    }

    protected function markNodeAsTraveled($node)
    {
        if (! $this->nodesTraveled->contains($node)) {
            $this->nodesTraveled->push($node);
        }
    }

    protected function addNextNodes($node)
    {
        if (! $this->nodesTraveled->contains($node) && 
            ! $this->nextNodes->contains($node))
        {
            $this->nextNodes->push($node);
        }
    }

    protected function addChild($node, $relation, $child)
    {
        $this->humData->addNode($child);
        $this->humData->addTo($node, $relation, $child);
        $this->addNextNodes($child);
    }

    protected function getRules($type)
    {
        $rules = $this->getAllRules();

        if (! array_key_exists($type, $rules)) {
            return [];
        }

        return $rules[$type];
    }

    protected function getAllRules()
    {
        return [
            'IpAddress' => [
                'connectionAssociated' => [
                    'type' => 'Connection',
                    'as' => 'originatingIp|respondingIp',
                    'relationship' => 'many',
                ],
                'alertAssociated' => [
                    'type' => 'Alert', 
                    'as' => 'sourceIp|destinationIp', 
                    'relationship' => 'many'
                ],
            ],
            'Connection' => [
                'originatingIp' => 'IpAddress',
                'respondingIp' => 'IpAddress',
                'serviceAssociated' => [ 
                    'type' => 'DHCP|DNP3|DNS|FTP|HTTP|SMTP|SSH|SSL', 
                    'as' => 'connectionAssociated', 
                    'relationship' => 'many'
                ],
            ],
        ];
    }
}