<?php

namespace App\SemanticEngine\NodeExpanders;

interface NodeExpander
{
    public function expand();
}