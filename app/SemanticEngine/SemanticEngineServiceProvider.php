<?php

namespace App\SemanticEngine;

use App;
use App\SemanticEngine\Factories\FullEngineFactory;
use App\SemanticEngine\SemanticEngine;
use Illuminate\Support\ServiceProvider;

class SemanticEngineServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        App::bind('semanticengine', function()
        {
            return new SemanticEngine(new FullEngineFactory);
        });
    }
}

