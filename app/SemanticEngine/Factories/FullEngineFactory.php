<?php

namespace App\SemanticEngine\Factories;

use App\SemanticEngine\Fetchers\FallbackFetcher;
use App\SemanticEngine\Inferences\Inferrer;
use App\SemanticEngine\NodeExpanders\SemanticGraphNodeExpander;
use App\SemanticEngine\NodeGetters\SemanticGraphNodeGetter;
use App\SemanticEngine\Sections\Presenter;
use App\SemanticEngine\TypeGuessers\LevenshteinTypeGuesser;
use App\SemanticEngine\TypeGuessers\RegexTypeGuesser;
use Log;

class FullEngineFactory implements ComponentFactory
{
    public function createTypeGuesser()
    {
        return new LevenshteinTypeGuesser(new RegexTypeGuesser);
    }
    
    public function createFetcher($query) 
    {
        $type = $query->get('type');
        $className = "App\SemanticEngine\Fetchers\\$type\\Fetcher";

        if (!is_null($type) && class_exists($className)) {
            return new $className($query);
        } else {
            return new FallbackFetcher($query);
        }
    }
    
    public function createParser($type)
    {
        $arr = explode(".", $type);
        if (count($arr) == 1) {
            $parserFolder = "";
            $parserName = studly_case($arr[0]);
        } elseif (count($arr) >= 2) {
            $parserFolder = studly_case($arr[0]) . "\\";
            $parserName = studly_case($arr[1]);
        } else {
            self::logInvalidParserTypeGiven($type);
            return null;
        }
        $parserClass = "App\SemanticEngine\Parsers\\$parserFolder${parserName}Parser";

        if (! class_exists($parserClass)) {
            self::logParserNotFound($type);
            return null;
        }

        return new $parserClass;
    }
    
    public function createNodeGetter($semData, $query)
    {
        return new SemanticGraphNodeGetter($semData, $query);
    }
    
    public function createNodeExpander($semData, $node)
    {
        return new SemanticGraphNodeExpander($semData, $node);
    }
    
    public function createInferrer($humData)
    {
        return Inferrer::create($humData);
    }
    
    public function createPresenter($humData)
    {
        $type = $humData->getRootType();
        $className = "App\SemanticEngine\Inferences\\$type\\Presenter";
        if (!is_null($type) && class_exists($className)) {
            return new $className($humData);
        } else {
            return new Presenter($humData);
        }
    }

    private static function logInvalidParserTypeGiven($parserType) {
        $type = gettype($parserType);
        Log::error("Invalid parser type given. String expected, $type given.");
    }

    private static function logParserNotFound($parserType) {
        Log::warning("The parser type '$parserType' could not be found.");
    }
}