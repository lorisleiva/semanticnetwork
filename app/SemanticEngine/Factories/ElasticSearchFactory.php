<?php

namespace App\SemanticEngine\Factories;

use App\SemanticEngine\Fetchers\FallbackFetcher;
use App\SemanticEngine\NodeExpanders\ElasticSearchNodeExpander;
use App\SemanticEngine\NodeGetters\ElasticSearchNodeGetter;
use App\SemanticEngine\TypeGuessers\ElasticSearchTypeGuesser;

class ElasticSearchFactory extends FullEngineFactory
{
    public function createTypeGuesser()
    {
        return new ElasticSearchTypeGuesser(parent::createTypeGuesser());
    }

    public function createFetcher($query) 
    {
        return new FallbackFetcher($query);
    }
    
    public function createNodeGetter($semData, $query)
    {
        return new ElasticSearchNodeGetter($query);
    }
    
    public function createNodeExpander($semData, $node)
    {
        return new ElasticSearchNodeExpander($node);
    }
}