<?php

namespace App\SemanticEngine\Factories;

interface ComponentFactory
{
    public function createTypeGuesser();
    public function createFetcher($query);
    public function createParser($type);
    public function createNodeGetter($semData, $query);
    public function createNodeExpander($semData, $node);
    public function createInferrer($humData);
    public function createPresenter($humData);
}