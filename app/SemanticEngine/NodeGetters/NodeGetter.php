<?php

namespace App\SemanticEngine\NodeGetters;

interface NodeGetter
{
    public function get();
}