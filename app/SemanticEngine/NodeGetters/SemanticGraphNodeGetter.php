<?php

namespace App\SemanticEngine\NodeGetters;

use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Data\SemanticData;

class SemanticGraphNodeGetter implements NodeGetter
{
    protected $semData;
    protected $query;

    function __construct($semData, $query)
    {
        $this->semData = $semData;
        $this->query = $query;
    }

    public function get()
    {
        // return null;
        
        $search = $this->query->get("search");

        if ($this->queryIsAnUID($search)) {
            return new Node($search);
        }

        $relatedNodes = $this->semData
            ->nodesWhereDestinationMatches($search);

        if (empty($relatedNodes)) {
            return null;
        }

        return new Node($relatedNodes[0]);
    }

    private function queryIsAnUID($search)
    {
        return $this->semData->hasSource($search);
    }
}