<?php

namespace App\SemanticEngine\NodeGetters;

use App\ElasticSearch\NodeAdapter;
use ES;

class ElasticSearchNodeGetter implements NodeGetter
{
    protected $query;

    function __construct($query)
    {
        $this->query = $query;
    }

    public function get()
    {
        $esData = ES::search(
            $this->query->get("search"),
            $this->query->get("type")
        )->first();

        if (empty($esData)) {
            return null;
        }

        return new NodeAdapter($esData);
    }
}