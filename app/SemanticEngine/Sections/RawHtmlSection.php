<?php

namespace App\SemanticEngine\Sections;

abstract class RawHtmlSection extends Section
{
    function __construct()
    {
        parent::__construct("raw-html");
        $this->data = "";
    }

    protected function addHTML($html)
    {
        $this->data .= $html;
    }

    protected function addText($text, $parseStyle = true)
    {
        if ($parseStyle) {
            $text = $this->parseStyle($text);
        }

        $this->addHTML($text);
    }

    protected function addParagraph($text, $parseStyle = true)
    {
        $this->addText("<p>$text</p>", $parseStyle);
    }

    protected function addDescription($start, array $phrases, $parseStyle = true)
    {
        $description = $this->parseDescription($start, $phrases);
        
        return $this->addParagraph($description, $parseStyle);
    }
}