<?php

namespace App\SemanticEngine\Sections;

use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Data\HumanData;

abstract class TitleSection extends Section
{
    function __construct()
    {
        parent::__construct("title");
    }

    protected function setTitle($title)
    {
        $this->data->put('title', $title);
    }

    protected function setTag($tag)
    {
        $this->data->put('tag', $tag);
    }

    protected function setTitleAndTag(Node $node, $property = null)
    {
        $title = $node->has($property) ? $node->get($property) : $node->getUid();
        $this->data->put('title', $title);
        $this->data->put('tag', $node->getType());
    }

    protected function canBeTitled(HumanData $humData, $property = null)
    {
        $root = $humData->getRoot();
        return $root && ($root->has($property) || $root->getUid());
    }

    protected function setDescription($description)
    {
        $this->data->put('description', $description);
    }
}