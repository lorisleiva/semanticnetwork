<?php

namespace App\SemanticEngine\Sections;

use App\SemanticEngine\GadgetNotation;

class Presenter
{
    use GadgetNotation;

    protected $gadgetPath = "App/SemanticEngine/Sections";
    protected $gadgetDefaultFolder = null;
    protected $humData;
    protected $sections;
    protected $fallbackSection;

    function __construct($humData)
    {
        $this->gadgetDefaultFolder = $humData->getRootType();
        $this->humData = $humData;
        $this->fallbackSection = new FallbackSection;
    }

    public function present()
    {
        $output = collect([]);

        foreach ($this->getSections() as $section) {
            if ($section->validate($this->humData)) {
                $section->handle($this->humData);
                $output->push($section->export());
            }
        }

        $this->addFallbackSectionIfNecessary($output);
        return $output->sortByDesc('priority');
    }

    private function addFallbackSectionIfNecessary($output)
    {
        if (! $output->isEmpty() && $output->pluck('priority')->max() > 0) {
            return;
        }

        if (! $this->fallbackSection->validate($this->humData)) {
            return;
        }

        $this->fallbackSection->handle($this->humData);
        $output->push($this->fallbackSection->export()); 
    }
}