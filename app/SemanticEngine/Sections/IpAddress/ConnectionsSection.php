<?php

namespace App\SemanticEngine\Sections\IpAddress;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\TableSection;
use Carbon\Carbon;

class ConnectionsSection extends TableSection
{
    protected $priority = 0;
    
    public function validate(HumanData $humData)
    {
        return $humData->getRoot()->has("connectionAssociated");
    }

    public function handle(HumanData $humData)
    {
        $connections = $humData->getRoot()->get("connectionAssociated");

        $this->addHeader('isOriginating', 'is initiator');
        $this->addHeader('port');
        $this->addHeader('otherIp', 'Other IP');
        $this->addHeader('protocol');
        $this->addHeader('service');
        $this->addHeader('bytesTransfered');
        $this->addHeader('startedAt');
        $this->addHeader('duration');
        $this->addHeader('status');

        $rows = collect([]);
        foreach ($connections as $connection) {
            $row = $connection->getContent()->toArray();
            $this->handleRow($row);
            $rows->push($row);
        }

        $this->data->put('rows', $rows->sortBy('timestamp'));
        $this->addConfig('title', 'Connections');
    }

    private function handleRow(&$row)
    {
        $row['timestamp'] = $row['startedAt'];
        $row['startedAt'] = $this->parseTime($row['startedAt']);

        if (isset($row['duration'])) 
        {
            $row['duration'] = $this->getHumanDuration($row['duration']);
        }

        if (isset($row['originatingBytes']) &&
            isset($row['respondingBytes']) &&
            isset($row['isOriginating'])) 
        {
            $bytes = $this->getIcon('download') . ' ';
            $bytes .= $this->parseBytes($row['isOriginating'] ? 
                $row['respondingBytes'] : 
                $row['originatingBytes']);

            $bytes .= ' ' . $this->getIcon('upload') . ' ';
            $bytes .= $this->parseBytes($row['isOriginating'] ? 
                $row['originatingBytes'] : 
                $row['respondingBytes']);

            $row['bytesTransfered'] = $bytes;
        }
    }

    private function getHumanDuration($duration)
    {
        $duration = explode(".", $duration);
        $seconds = (int) $duration[0];
        $millis = (int) substr($duration[1], 0, 2);
        if ($seconds > 60) {
            $minutes = $seconds / 60;
            $seconds = $seconds % 60;
        } else {
            $minutes = 0;
        }

        $str = $minutes > 0 ? "$minutes min " : "";
        $str = $seconds > 0 ? "$seconds sec " : "";
        $str = $millis > 0 ? "$millis ms " : "";
        $str = trim($str);

        return $str === '' ? '<1 ms' : $str;
    }

    private function parseTime($time)
    {
        if (is_numeric($time)) {
            $carbon = Carbon::createFromTimestamp($time);
        } else {
            $carbon = Carbon::parse($time);
        }

        return $carbon
            ->setTimezone('Europe/Amsterdam')
            ->format('d/m/Y H:i:s');
    }
}