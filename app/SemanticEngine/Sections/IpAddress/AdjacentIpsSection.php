<?php

namespace App\SemanticEngine\Sections\IpAddress;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\TableSection;
use Carbon\Carbon;

class AdjacentIpsSection extends TableSection
{
    protected $priority = 50;

    public function validate(HumanData $humData)
    {
        return $humData->getRoot()->has("adjacentIps");
    }

    public function handle(HumanData $humData)
    {
        $adjacentIps = $humData->getRoot()
            ->get("adjacentIps")
            ->sortByDesc('connections');

        $this->addHeader('ip');
        $this->addHeader('connections');
        $this->addHeader('bytesTransfered');
        $this->addHeader('myPorts');
        $this->addHeader('otherPorts');

        foreach ($adjacentIps as $ip => $stats) {
            $row = $stats->toArray();
            $row['ip'] = $this->wrapLink($ip, 'IpAddress');
            $this->handleRow($row);
            $this->addRow($row);
        }

        $this->addConfig('title', 'Connected IPs');
    }

    private function handleRow(&$row)
    {
        $bytes = $this->getIcon('download') . ' ';
        $bytes .= $this->parseBytes($row['bytesDownloaded']);
        $bytes .= ' ' . $this->getIcon('upload') . ' ';
        $bytes .= $this->parseBytes($row['bytesUploaded']);
        $row['bytesTransfered'] = $bytes;

        $row['myPorts'] = $this->handlePorts($row['myPorts']);
        $row['otherPorts'] = $this->handlePorts($row['otherPorts']);
    }

    public function handlePorts(array $ports)
    {
        $maxPorts = 3;

        if (count($ports) <= $maxPorts) {
            return implode(', ', $ports);
        }

        $plusText = '[+' . (count($ports) - $maxPorts) . ']';
        $plusDetails = implode(', ', array_slice($ports, $maxPorts));
        $plusSpan = $this->addDetails($plusText, $plusDetails);

        return implode(', ', array_slice($ports, 0, $maxPorts)) . ' ' . $plusSpan;
    }

    private function getHumanDuration($duration)
    {
        $duration = explode(".", $duration);
        $seconds = (int) $duration[0];
        $millis = (int) substr($duration[1], 0, 2);
        if ($seconds > 60) {
            $minutes = $seconds / 60;
            $seconds = $seconds % 60;
        } else {
            $minutes = 0;
        }

        $str = $minutes > 0 ? "$minutes min " : "";
        $str = $seconds > 0 ? "$seconds sec " : "";
        $str = $millis > 0 ? "$millis ms " : "";
        $str = trim($str);

        return $str === '' ? '<1 ms' : $str;
    }
}