<?php

namespace App\SemanticEngine\Sections\IpAddress;

use App\SemanticEngine\Data\Data;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\Section;

class TimelineSection extends Section
{
    /* Number of minimum seconds that separates batches of connections */
    const TIMESTAMP_GRAIN = 60;

    protected $priority = 40;

    function __construct()
    {
        parent::__construct("timeline");
    }

    public function validate(HumanData $humData)
    {
        // return false; // Live Demo
        return $humData->getRoot()->hasType("IpAddress") 
            && $humData->getRoot()->hasNode("connectionAssociated");
    }

    public function handle(HumanData $humData)
    {
        $connections = $this->getConnectionsOrderedByTimestamp($humData->getRoot());
        $timeline = collect([]);

        foreach ($connections as $connection) {

            $closestBatch = $this->getClosestBatch($connection, $timeline);
            $batch = $this->createOrMergeBatch($connection, $closestBatch, $timeline);

            $this->incrementConnectionNbr($batch);
            $this->addOtherIps($connection, $batch);
        }

        foreach ($timeline as $batch) {
            $this->addAlerts($humData->getRoot(), $batch);
            $otherIps = collect($batch->get('otherIps', []))
                ->sortBy(function($data, $ip){
                    return $ip;
                });
            $batch->put('otherIps', $otherIps);
        }

        $this->data = $timeline;
        $this->addConfig('title', 'Timeline');
    }

    private function getConnectionsOrderedByTimestamp($node)
    {
        $connections = collect($node->get('connectionAssociated'));

        return $connections->sortBy(function($connection) {
            return date_timestamp_get(date_create($connection->get('startedAt')));
        });
    }

    private function getTimestamp($datetime)
    {
        return date_timestamp_get(date_create($datetime));
    }

    private function getMinDistance($connection, $batch)
    {
        $startA = $this->getTimestamp($connection->get('startedAt'));
        $endA = $startA + $connection->get('duration');
        $startB = $batch->get('start');
        $endB = $startB + $batch->get('duration');

        if ($endA < $startB) {
            return $startB - $endA;
        }
        if ($endB < $startA) {
            return $startA - $endB;
        }

        return 0;
    }

    private function mergeTimestampsToBatch($connection, &$batch)
    {
        $startA = $this->getTimestamp($connection->get('startedAt'));
        $endA = $startA + $connection->get('duration');
        $startB = $batch->get('start');
        $endB = $startB + $batch->get('duration');

        $startC = min($startA, $startB);
        $endC = max($endA, $endB);
        $durationC = $endC - $startC;

        $batch->put('start', $startC);
        $batch->put('duration', $durationC);
    }

    private function getClosestBatch($connection, $timeline)
    {
        $closestBatch = null;
        $distanceToClosestBatch = null;

        foreach ($timeline as $batch) {
            $distance = $this->getMinDistance($connection, $batch);

            if (! $distanceToClosestBatch || $distance < $distanceToClosestBatch) {
                $distanceToClosestBatch = $distance;
                $closestBatch = $batch;
            }
        }

        return $closestBatch;
    }

    private function createOrMergeBatch($connection, &$batch, &$timeline)
    {
        if ($batch && $this->getMinDistance($connection, $batch) < self::TIMESTAMP_GRAIN) {
            $this->mergeTimestampsToBatch($connection, $batch);
        } else {
            $batch = collect([
                'start' => $this->getTimestamp($connection->get('startedAt')),
                'duration' => $connection->get('duration')
            ]);
            $timeline->push($batch);
        }

        return $batch;
    }

    private function incrementConnectionNbr(&$batch)
    {
        $batch->put('connections', $batch->get('connections', 0) + 1);
    }

    private function addOtherIps($connection, &$batch)
    {
        $otherIps = $batch->get('otherIps') ?: collect();
        $otherIp = $this->getOtherIpFromConnection($connection);
        $otherIpData = $otherIps->get($otherIp) ?: collect([
            'services' => collect(),
            'connections' => 0,
            'initiating' => 0
        ]);

        $isInitiating = $connection->get(Data::AS_RELATION) === 'originatingIp';
        $newServices = collect($connection->get('serviceAssociated', []))
            ->map(function($service) { return $service->get(Data::TYPE_RELATION); })
            ->merge($otherIpData->get('services'))
            ->unique()
            ->values();

        $otherIpData->put('services', $newServices);
        $otherIpData->put('connections', $otherIpData->get('connections') + 1);
        $otherIpData->put('initiating', $otherIpData->get('initiating') + (int) $isInitiating);

        $otherIps->put($otherIp, $otherIpData);
        $batch->put('otherIps', $otherIps);
    }

    private function addAlerts($node, $batch)
    {
        $alerts = collect($node->get('alertAssociated', []))
            ->filter(function($alert) use ($batch) {
                $alertTimestamp = $this->getTimestamp($alert->get('generatedAt'));
                $batchStart = $batch->get('start');
                $batchEnd = $batch->get('start') + $batch->get('duration');
                return $alertTimestamp >= $batchStart && $alertTimestamp <= $batchEnd;
            })
            ->map(function($alert) { return [
                'message' => $alert->get('message'),
                'subMessage' => $alert->get('subMessage'),
                'note' => $alert->get('note'),
            ]; })
            ->values();

        $batch->put('alerts', $alerts);
    }

    private function getOtherIpFromConnection($connection)
    {
        if ($connection->get(Data::AS_RELATION) === 'originatingIp') 
        {
            $otherIp = $connection->get('respondingIp');
        } 
        else 
        {
            $otherIp = $connection->get('originatingIp');
        }

        if (is_array($otherIp) && count($otherIp) == 1 && $otherIp[0]->has('@uid'))
        {
            $otherIp = explode("/", $otherIp[0]->getUid());
            $otherIp = array_pop($otherIp);
            $otherIp = preg_replace("/-/", ".", $otherIp);
        }
        elseif (is_string($otherIp)) 
        {
            $otherIp = substr($otherIp, 2);
        }

        if (is_array($otherIp) && ! empty($otherIp)) 
        {
            $otherIp = $otherIp[0];
            $otherIp = $otherIp->get('ipv4');
        }

        return $otherIp;
    }
}