<?php

namespace App\SemanticEngine\Sections\IpAddress;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\TableSection;

class FtpSection extends TableSection
{
    protected $priority = 10;

    protected $ftpServices;
    
    public function validate(HumanData $humData)
    {
        // return false; // Live Demo
        $connections = $humData->getRoot()->get("connectionAssociated");

        if (! $connections) {
            return false;
        }

        $this->ftpServices = $this->getFtpServices($connections);
        return $connections && ! $this->ftpServices->isEmpty();
    }

    public function handle(HumanData $humData)
    {
        $this->addHeader('connectionAssociated', 'Connection');
        $this->addHeader('command');
        $this->addHeader('argument');
        $this->addHeader('replyCode');
        $this->addHeader('replyMessage');

        $this->data->put('rows', $this->ftpServices);
        $this->addConfig('title', 'FTP');
    }

    private function getFtpServices($connections)
    {
        $ftpServices = collect();

        foreach ($connections as $connection) {
            $services = collect($connection->get('serviceAssociated'))
                ->filter(function($service) {
                    return $service->getType() === "FTP";
                })
                ->map(function($service) {
                    return $service->getContent()->toArray();
                });

            if (! $services->isEmpty()) {
                $ftpServices = $ftpServices->merge($services);
            }
        }

        return $ftpServices;
    }
}