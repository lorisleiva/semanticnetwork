<?php

namespace App\SemanticEngine\Sections\IpAddress;

use App\ElasticSearch\ElasticSearchClient;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Sections\RawHtmlSection;
use App\SemanticEngine\Sections\TitleSection as ParentTitleSection;

class TitleSection extends ParentTitleSection
{
    protected $priority = 100;

    public function validate(HumanData $humData)
    {
        return $this->canBeTitled($humData, "ipv4");
    }

    public function handle(HumanData $humData)
    {
        $root = $humData->getRoot();
        $this->setTitleAndTag($root, "ipv4");
        $this->setDescription($this->generateDescription($root));
    }

    private function generateDescription(Node $root)
    {
        $ip = $root->get("ipv4");
        $start = "The IP address **$ip**";
        $phrases = [];

        if ($root->has('isLocal'))
        {
            $isLocal = $root->get('isLocal');
            $phrases[] = ($isLocal ? 'is' : 'isn\'t') . ' local';
        }

        if ($root->has('connectionAssociated')) 
        {
            $connectionsNbr = $root->get('connectionAssociatedNumber');
            $phrase = "has $connectionsNbr connections";

            if ($root->has('adjacentIps')) {
                $ips = $root->get('adjacentIps');
                $phrase .= ' with ' . count($ips) . ' unique IP addresses';
            }

            $phrases[] = $phrase;
        }

        return $this->parseDescription($start, $phrases);
    }
}