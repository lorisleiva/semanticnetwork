<?php

namespace App\SemanticEngine\Sections;

use App\SemanticEngine\Data\Node;

abstract class TableSection extends Section
{
    function __construct()
    {
        parent::__construct("table");
        $this->data = collect([
            'headers' => collect([]),
            'rows' => collect([])
        ]);
    }

    protected function addHeader($key, $header = null)
    {
        if (is_null($header)) {
            $header = title_case(preg_replace('/_/', ' ', snake_case($key)));
        }
        $this->data->get('headers')->put($key, $header);
    }

    protected function addRow($row)
    {
        $this->data->get('rows')->push($row);
    }

    protected function autoGenerateTable(array $array, array $except = [])
    {
        $headersAdded = collect([]);
        $except = collect($except);

        foreach ($array as $item) {

            if ($item instanceof Node) {
                $item = $item->getContent()->toArray();
            }

            if (! is_array($item)) {
                continue;
            }

            foreach ($item as $key => $value) {
                if (! $headersAdded->contains($key) && 
                    ! $except->contains($key))
                {
                    $headersAdded->push($key);
                    $this->addHeader($key);
                }
            }

            $this->addRow($item);
        }
    }

    protected function wrapLink($uid, $type, $content = null)
    {
        return [
            'content' => $content ?: $uid,
            'type' => $type,
            'id' => $uid,
        ];
    }
}