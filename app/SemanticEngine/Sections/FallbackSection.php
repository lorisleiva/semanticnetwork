<?php

namespace App\SemanticEngine\Sections;

use App\SemanticEngine\Data\HumanData;

class FallbackSection extends RawHtmlSection
{
    protected $priority = -100;

    public function validate(HumanData $humData)
    {
        return $humData->getRoot();
    }

    public function handle(HumanData $humData)
    {
        $nodeInfo = $this->getNodeInfo($humData);
        $text = "<p>...but no section has been written for it. Here is the information it contains:</p>";
        $text .= "<blockquote>$nodeInfo</blockquote>";

        $this->data .= "<div class='content'>$text</div>";
        $this->addConfig('title', 'A node has been found...');
    }

    private function getNodeInfo(HumanData $humData)
    {
        $nodeInfo = [];
        $nodeContent = $humData->getRoot()->getContent();

        foreach ($nodeContent as $field => $value) {
            if (is_string($value) || is_numeric($value)) {
                $parsedValue = $value;
            } else if(is_bool($value)) {
                $parsedValue = $value ? 'true' : 'false';
            } else {
                $parsedValue  = gettype($value);
                $parsedValue .= is_array($value) ? ' (' . count($value) . ')' : '';
                $parsedValue  = "<em>$parsedValue</em>";
            }

            $nodeInfo[] = "<strong>$field</strong> = $parsedValue";
        }

        return implode('<br>', $nodeInfo);
    }
}