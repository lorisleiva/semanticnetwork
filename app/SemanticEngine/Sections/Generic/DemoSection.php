<?php

namespace App\SemanticEngine\Sections\Generic;

use SemanticEngine;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\RawHtmlSection;

class DemoSection extends RawHtmlSection
{
    protected $priority = -50;
    protected $intermediateDataTitle;
    protected $intermediateData;

    public function validate(HumanData $humData)
    {
        return false;
    }

    public function handle(HumanData $humData)
    {
        $this->getDataToDisplay($humData);
        $this->addConfig('title', 'Demo: ' . $this->intermediateDataTitle);
        $this->addHtml("<pre>$this->intermediateData</pre>");
    }

    private function getDataToDisplay(HumanData $humData)
    {
        $previousData = SemanticEngine::getPreviousData();
        $rootNode = $humData->getRoot();

        if ($rootNode && ! $rootNode->getChildren()->isEmpty()) {
            $this->intermediateDataTitle = "Human Data";
            $this->intermediateData = $humData;
        } elseif ($rootNode) {
            $this->intermediateDataTitle = "Node";
            $this->intermediateData = $rootNode;
        } elseif (! $previousData['semData']->isEmpty()) {
            $this->intermediateDataTitle = "Semantic Data";
            $this->intermediateData = $previousData['semData'];
        } elseif (! $previousData['rawData']->isEmpty()) {
            $this->intermediateDataTitle = "Raw Data";
            $this->intermediateData = print_r($previousData['rawData']->toArray(), true);
        } else {
            $this->intermediateDataTitle = "Query";
            $this->intermediateData = print_r($previousData['query']->toArray(), true);
        }
    }
}