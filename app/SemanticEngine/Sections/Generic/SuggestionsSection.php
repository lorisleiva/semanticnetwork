<?php

namespace App\SemanticEngine\Sections\Generic;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\Section;

class SuggestionsSection extends Section
{
    protected $priority = -101;

    function __construct()
    {
        parent::__construct("suggestions");
    }

    public function validate(HumanData $humData)
    {
        return $humData->hasSuggestions();
    }

    public function handle(HumanData $humData)
    {
        $this->data = $humData
            ->getSuggestions()
            ->map(function($item){
                return array_only($item, ['id', 'title', 'type']);
            });

        $this->addConfig('title', 'Research suggestions');
    }
}