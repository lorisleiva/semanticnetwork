<?php

namespace App\SemanticEngine\Sections\Person;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\TitleSection as ParentTitleSection;

class TitleSection extends ParentTitleSection
{
    protected $priority = 100;

    public function validate(HumanData $humData)
    {
        return $this->canBeTitled($humData, "name");
    }

    public function handle(HumanData $humData)
    {
        $root = $humData->getRoot();
        $this->setTitleAndTag($root, "name");

        $sentences = [];

        if ($root->has('address')) {
            $address = $root->get('address')[0];
            $city = $address->get('addressLocality');
            $country = $address->get('addressCountry');

            $location = $country ? "$city, $country" : $city;
            $sentences[] = "lives in $location";
        }

        if ($root->has('url')) {
            $website = $root->get('url');
            $sentences[] = "has the following website: <a href='$website'>$website</a>";
        }

        $start = $root->get('givenName') ?: $root->get('name');
        return $this->setDescription($this->parseDescription($start, $sentences));
    }
}