<?php

namespace App\SemanticEngine\Sections\File;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Sections\TitleSection as ParentTitleSection;

class TitleSection extends ParentTitleSection
{
    protected $priority = 100;

    public function validate(HumanData $humData)
    {
        return $this->canBeTitled($humData, "filename");
    }

    public function handle(HumanData $humData)
    {
        $root = $humData->getRoot();
        $this->setTitleAndTag($root, "filename");
    }
}