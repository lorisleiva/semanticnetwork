<?php

namespace App\SemanticEngine\Sections;

use App\SemanticEngine\Data\HumanData;

abstract class Section
{
    protected $priority = 0;
    protected $config;
    protected $data;

    abstract public function validate(HumanData $humData);
    abstract public function handle(HumanData $humData);

    function __construct($type = 'raw-html')
    {
        $this->type = $type;
        $this->config = collect([]);
        $this->data = collect([]);
    }

    public function addConfig($key, $value)
    {
        $this->config->put($key, $value);
    }

    public function export()
    {
        $section = collect($this->config);
        $section->put("data", $this->data);
        $section->put("type", $this->type);
        $section->put("priority", $this->priority);
        return $section;
    }

    protected function parseStyle($text)
    {
        $text = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $text);
        $text = preg_replace('#\*(.*?)\*#', '<i>$1</i>', $text);
        $text = preg_replace('#_(.*?)_#', '<u>$1</u>', $text);

        return $text;
    }

    protected function getIcon($icon = "search")
    {
        return "<div class='icon'><span class='fa fa-$icon'></span></div>";
    }

    protected function addDetails($text, $details)
    {
        return "<abbr title='$details'>$text</abbr>";
    }

    protected function parseBytes($bytes)
    {
        $bytes = (float) $bytes;
        if ($bytes < 1e3) {
            return (string) $bytes;
        } elseif ($bytes < 1e6) {
            return round($bytes / 1e3, 2) . "KB";
        } elseif ($bytes < 1e9) {
            return round($bytes / 1e6, 2) . "MB";
        } else {
            return round($bytes / 1e9, 2) . "GB";
        }
    }

    protected function parseDescription($start, array $phrases)
    {
        $description = $start . ' ';

        if (count($phrases) == 0) {
            $description .= "doesn't have many properties.";
        } elseif (count($phrases) == 1) {
            $description .= $phrases[0];
        } else {
            $finalPhrase = array_pop($phrases);
            $description .= implode(", ", $phrases) . " and $finalPhrase";
        }

        return $this->parseStyle($description);
    }
}