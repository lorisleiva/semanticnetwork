<?php

namespace App\SemanticEngine\Inferences\IpAddress;

use ES;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class ConnectionNumberInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->hasType("IpAddress")
            && $node->hasNode("connectionAssociated")
            && ! $node->has("connectionAssociatedNumber");
    }

    public function handle(Node $node, HumanData $humData)
    {
        if (ES::canBeUsed()) {
            $number = $this->countFromElasticSearch($node);
        } else {
            $number = $this->countFromNode($node);
        }

        $node->add('connectionAssociatedNumber', $number);
    }

    private function countFromNode(Node $node)
    {
        $connections = $node->get("connectionAssociated");
        return count($connections);
    }

    private function countFromElasticSearch(Node $node)
    {
        $uid = $node->getUid();
        $query_string = "originatingIp:$uid OR respondingIp:$uid";
        return ES::search($query_string, 'Connection')->count();
    }
}