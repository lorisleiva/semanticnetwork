<?php

namespace App\SemanticEngine\Inferences\IpAddress;

use App\SemanticEngine\Data\Data;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class AdjacentIpsInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->hasType("IpAddress")
            && $node->hasNode("connectionAssociated")
            && ! $node->has("adjacentIps");
    }

    public function handle(Node $node, HumanData $humData)
    {
        $connections = $node->get('connectionAssociated');
        $ips = collect([]);


        foreach ($connections as $connection) {
            $otherIp = $this->getOtherIpFromConnection($connection);

            if (! $ips->has($otherIp)) 
            {
                $ips->put($otherIp, collect([
                    'bytesDownloaded' => 0,
                    'bytesUploaded' => 0,
                    'connections' => 0,
                    'myPorts' => collect([]),
                    'otherPorts' => collect([]),
                ]));
            }

            $stats = $ips->get($otherIp);
            $this->incrementConnectionNbr($stats);
            $this->updateBytes($connection, $stats);
            $this->updatePorts($connection, $stats);

        }

        $node->add("adjacentIps", $ips);
    }

    private function getOtherIpFromConnection($connection)
    {
        if ($connection->get(Data::AS_RELATION) === 'originatingIp') 
        {
            $otherIp = $connection->get('respondingIp');
        } 
        else 
        {
            $otherIp = $connection->get('originatingIp');
        }

        if (is_array($otherIp) && count($otherIp) == 1 && $otherIp[0]->has('@uid'))
        {
            $otherIp = explode("/", $otherIp[0]->getUid());
            $otherIp = array_pop($otherIp);
            $otherIp = preg_replace("/-/", ".", $otherIp);
        }
        elseif (is_string($otherIp)) 
        {
            $otherIp = substr($otherIp, 2);
        }

        if (is_array($otherIp) && ! empty($otherIp)) 
        {
            $otherIp = $otherIp[0];
            $otherIp = $otherIp->get('ipv4');
        }

        return $otherIp;
    }

    private function updateBytes($connection, &$stats)
    {
        if ($connection->get(Data::AS_RELATION) === 'originatingIp') 
        {
            $newBytesDownloaded = 
                $stats->get('bytesDownloaded') + 
                $connection->get('respondingBytes');

            $newBytesUploaded = 
                $stats->get('bytesUploaded') + 
                $connection->get('originatingBytes');
        } 
        else 
        {
            $newBytesDownloaded = 
                $stats->get('bytesDownloaded') + 
                $connection->get('originatingBytes');

            $newBytesUploaded = 
                $stats->get('bytesUploaded') + 
                $connection->get('respondingBytes');
        }
        $stats->put('bytesDownloaded', $newBytesDownloaded);
        $stats->put('bytesUploaded', $newBytesUploaded);
    }

    private function incrementConnectionNbr(&$stats)
    {
        $stats->put('connections', $stats->get('connections') + 1);
    }

    private function updatePorts($connection, &$stats)
    {
        if ($connection->get(Data::AS_RELATION) === 'originatingIp') 
        {
            $newMyPort = $connection->get('originatingPort');
            $newOtherPort = $connection->get('respondingPort');
        } 
        else 
        {
            $newMyPort = $connection->get('respondingPort');
            $newOtherPort = $connection->get('originatingPort');
        }

        $myPorts = $stats->get('myPorts');
        $otherPorts = $stats->get('otherPorts');
        
        if (! $myPorts->contains($newMyPort)) 
        {
            $myPorts->push($newMyPort);
        }
        
        if (! $otherPorts->contains($newOtherPort)) 
        {
            $otherPorts->push($newOtherPort);
        }
    }
}