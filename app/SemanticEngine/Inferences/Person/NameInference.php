<?php

namespace App\SemanticEngine\Inferences\Person;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class NameInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->has('name')
            && ! $node->has('givenName')
            && ! $node->has('familyName');
    }

    public function handle(Node $node, HumanData $humData)
    {
        $nameArray = explode(' ', $node->get("name"));

        if (count($nameArray) < 2) {
            $node->add('givenName', $nameArray[0]);
            return;
        }

        $node->add('givenName', array_shift($nameArray));
        $node->add('familyName', implode(' ', $nameArray));
    }
}