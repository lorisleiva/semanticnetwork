<?php

namespace App\SemanticEngine\Inferences;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;

abstract class Inference
{
    protected $timeUsed = 0;
    protected $affectedNodes;

    abstract public function validate(Node $node, HumanData $humData);
    abstract public function handle(Node $node, HumanData $humData);

    function __construct()
    {
        $this->affectedNodes = collect([]);
    }

    public function isValidNode(Node $node, HumanData $humData)
    {
        // TODO: Remove $node from $affectedNodes if changed since last time
        // Use $node->timestamp

        return !$this->affectedNodes->contains($node->getUid()) 
            && $this->validate($node, $humData);
    }

    public function handleNode(Node $node, HumanData $humData)
    {
        $this->handle($node, $humData);
        $this->affectNode($node->getUid());
    }

    public function incrementTimeUsed()
    {
        $this->timeUsed++;
    }

    private function affectNode($uid)
    {
        if (!$this->affectedNodes->contains($uid)) {
            $this->affectedNodes->push($uid);
        }
    }
}