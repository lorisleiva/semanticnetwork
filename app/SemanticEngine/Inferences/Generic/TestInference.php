<?php

namespace App\SemanticEngine\Inferences\Generic;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class TestInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->hasType("Test");
    }

    public function handle(Node $node, HumanData $humData)
    {
        //
    }
}