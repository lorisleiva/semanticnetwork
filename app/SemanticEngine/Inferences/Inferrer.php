<?php

namespace App\SemanticEngine\Inferences;

use App\SemanticEngine\GadgetNotation;

class Inferrer
{
    use GadgetNotation;

    protected $gadgetPath = "App/SemanticEngine/Inferences";
    protected $gadgetDefaultFolder = null;
    protected $inferenceMap;
    protected $humData;

    private function __construct($humData, $type)
    {
        $this->inferenceMap = collect([]);
        $this->humData = $humData;
        $this->gadgetDefaultFolder = $type;
    }

    public function infer()
    {
        $nodes = $this->humData->getNodesFromLeaves();
        $inferUntilNoneValidates = 
            ! property_exists($this, "inferUntilNoneValidates") 
            || ! is_bool($this->inferUntilNoneValidates) 
            || $this->inferUntilNoneValidates;

        do {
            $someValidates = false;
            foreach ($nodes as $node) {
                $nodeType = $node->getType();
                if (is_null($nodeType)) {
                    continue;
                }

                $inferences = $this->inferenceMap->get($nodeType);
                if (is_null($inferences)) {
                    $inferences = $this->getInferences($nodeType);
                    $this->inferenceMap->put($nodeType, $inferences);
                }

                foreach ($inferences as $inference) {
                    if ($inference->isValidNode($node, $this->humData)) {
                        $inference->handleNode($node, $this->humData);
                        $inference->incrementTimeUsed();
                        $someValidates = true;
                    }
                }
            }
        } while ($inferUntilNoneValidates && $someValidates);
    }

    public function getInferences($nodeType)
    {
        $rootType = $this->humData->getRootType();
        if (!is_null($nodeType) && $rootType !== $nodeType) {
            $newInferrer = self::create($this->humData, $nodeType);
            return $newInferrer->generateInferences();
        }
        return $this->generateInferences();
    }

    private function getDefaultInferences()
    {
        return collect([ "Generic:all", "all" ]);
    }

    static public function create($humData, $customType = null)
    {
        $type = is_null($customType) ? $humData->getRootType() : $customType;
        $className = "App\SemanticEngine\Inferences\\$type\\Inferrer";
        if (! is_null($type) && class_exists($className, false)) {
            return new $className($humData, $type);
        } else {
            return new Inferrer($humData, $type);
        }
    }
}