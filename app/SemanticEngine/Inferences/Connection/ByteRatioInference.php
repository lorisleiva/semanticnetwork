<?php

namespace App\SemanticEngine\Inferences\Connection;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class ByteRatioInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->hasType("Connection")
            && $node->has("originatingBytes")
            && $node->has("respondingBytes")
            && ! $node->has("ratioOrigRespBytes");
    }

    public function handle(Node $node, HumanData $humData)
    {
        $srcBytes = (int) $node->get("sourceBytes");
        $destBytes = (int) $node->get("destinationBytes");
        $totalBytes = $srcBytes + $destBytes;
        $srcBytes = $this->getPercent($srcBytes, $totalBytes);
        $destBytes = $this->getPercent($destBytes, $totalBytes);
        $node->add("ratioOrigRespBytes", "$srcBytes%/$destBytes%");
    }

    private function getPercent($value, $total)
    {
        if ($total == 0) {
            return 0;
        }
        return round(($value / $total) * 100);
    }
}