<?php

namespace App\SemanticEngine\Inferences\Connection;

use App\SemanticEngine\Data\Data;
use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Inferences\Inference;

class OriginatingRespondingInference extends Inference
{
    public function validate(Node $node, HumanData $humData)
    {
        return $node->hasType("Connection")
            && $node->has(Data::AS_RELATION)
            && (
                $node->get(Data::AS_RELATION) === "originatingIp" ||
                $node->get(Data::AS_RELATION) === "respondingIp"
            )
            && (
                $node->hasNode("originatingIp") ||
                $node->hasNode("respondingIp")
            )
            && ! $node->has("isOriginating")
            && ! $node->has("otherIp")
            && ! $node->has("port");
    }

    public function handle(Node $node, HumanData $humData)
    {
        // is originating
        $isOriginating = $node->get(Data::AS_RELATION) === "originatingIp";
        $node->add("isOriginating", $isOriginating);

        // current ip port
        $myPortField = $isOriginating ? "originatingPort" : "respondingPort";
        $myPort = $node->get($myPortField);
        $node->add("port", $myPort);

        $otherIp = $this->getOtherIpFromConnection($node);
        $otherPortField = $isOriginating ? "respondingPort" : "originatingPort";
        $otherPort = $node->get($otherPortField);
        $otherIp .= is_null($otherPort) ? '' : ":$otherPort";
        $node->add("otherIp", $otherIp);
    }

    private function getOtherIpFromConnection($connection)
    {
        if ($connection->get(Data::AS_RELATION) === 'originatingIp') 
        {
            $otherIp = $connection->get('respondingIp');
        } 
        else 
        {
            $otherIp = $connection->get('originatingIp');
        }

        $otherIp = $otherIp[0];

        if ($otherIp->has('ipv4')) 
        {
            $otherIp = $otherIp->get('ipv4');
        }
        else
        {
            $otherIp = explode("/", $otherIp->getUid());
            $otherIp = array_pop($otherIp);
            $otherIp = preg_replace("/-/", ".", $otherIp);
        }

        return $otherIp;
    }
}