<?php

namespace App\SemanticEngine\Parsers\Social;

use App\SemanticEngine\Parsers\Parser as BaseParser;

class TwitterParser extends BaseParser
{
    protected $url;
    protected $semData;
    protected $html;

    public function parseUrl($url, $semData)
    {
        $this->url = $url;
        $this->semData = $semData;
        $semData->define($url, 'Person');

        try {
            $this->html = file_get_html($url);
        } catch (\Exception $e) {
            return;
        }

        $this->parseName();
        $this->parseWebsite();
        $this->parseLocation();
    }

    private function parseName()
    {
        $name = $this->getTextFrom('.ProfileHeaderCard-nameLink');
        $this->put('name', title_case($name));
    }

    private function parseWebsite()
    {
        $website = $this->getLinkFrom('.ProfileHeaderCard-url a');

        if ($website) {
            $this->put('url', $website);
        }
    }

    private function parseLocation()
    {
        $location = $this->getTextFrom('.ProfileHeaderCard-location');

        if (! $location) {
            return;
        }

        $locationArray  = explode(', ', $location);
        $locationUrl    = $this->urlOf($location);
        $this->put('address', $locationUrl);

        if (count($locationArray) >= 3)
        {
            $this->putTo($locationUrl, 'addressRegion', $locationArray[1]);
            $this->putTo($locationUrl, 'addressCountry', $locationArray[2]);
        }
        elseif (count($locationArray) >= 2)
        {
            if (strlen($locationArray[1]) == 2) 
            {
                $this->putTo($locationUrl, 'addressRegion', $locationArray[1]);
                $this->putTo($locationUrl, 'addressCountry', 'USA');
            }
            else 
            {
                $this->putTo($locationUrl, 'addressCountry', $locationArray[1]);
            }
        } 

        $this->putTo($locationUrl, 'addressLocality', $locationArray[0]);
    }

    /**
     * Helper functions
     */

    private function put($relation, $value)
    {
        $this->putTo($this->url, $relation, $value);
    }

    private function putTo($node, $relation, $value)
    {
        $this->semData->add($node, $relation, $value);
    }

    private function getTextFrom($selector)
    {
        $el = $this->html->find($selector, 0);
        return $el ? trim($el->plaintext) : null;
    }

    private function getLinkFrom($selector)
    {
        $el = $this->html->find($selector, 0);
        return $el ? trim($el->title) : null;
    }

    private function urlOf($node)
    {
        $identifier = str_replace(',', '_', snake_case(strtolower($node)));
        return "http://wikidata.org/$identifier";
    }
}