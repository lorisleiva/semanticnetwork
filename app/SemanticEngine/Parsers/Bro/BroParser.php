<?php

namespace App\SemanticEngine\Parsers\Bro;

use App\SemanticEngine\Parsers\Parser;
use Carbon\Carbon;

abstract class BroParser extends Parser
{
    public static $logParsed = 0;
    protected $file_format = "CSV";
    protected $config = [];
    protected $semData;

    public function parseFile($rawData, $semData)
    {
        $this->semData = $semData;
        $fileFormatIdentified = false;

        $file = fopen($rawData, "rb") or die("Unable to open file!");
        while(!feof($file)) {
            $line = fgets($file);

            if (! $fileFormatIdentified) {
                $this->identifyFileFormat($line);
                $fileFormatIdentified = true;
            }
            
            $this->processLine($line);
        }
        fclose($file);

        return $this->semData;
    }

    public function parseFileGz($rawData, $semData)
    {
        $this->semData = $semData;
        $fileFormatIdentified = false;

        $file = gzopen($rawData, "rb") or die("Unable to open file!");
        while(!gzeof($file)) {
            $line = gzgets($file);

            if (! $fileFormatIdentified) {
                $this->identifyFileFormat($line);
                $fileFormatIdentified = true;
            }

            $this->processLine($line);
        }
        gzclose($file);

        return $this->semData;
    }

    public function processLine($line)
    {
        self::$logParsed++;
        call_user_func([$this, "processLine$this->file_format"], $line);
    }

    protected function processLineCSV($line)
    {
        if (strlen($line) == 0) {
            return;
        }

        if ($line[0] == '#') {
            return $this->processConfig(trim(substr($line, 1)));
        }

        // TODO unescape $this->config["separator"];
        $connectionData = explode("\x09", trim($line));

        $uri = $this->getURI($connectionData[$this->uidKey]);
        $this->semData->define($uri, $this->type);

        for ($i=0; $i < count($connectionData); $i++) { 
            $field = $this->config["fields"][$i];
            $value = $connectionData[$i];

            if ($this->shouldAddField($field, $value)) {
                $this->processField($uri, $field, $value);
            }
        }
    }

    protected function processLineJSON($line)
    {
        $data = json_decode($line, true);

        $uri = $this->getURI($data[$this->uidKey]);
        $this->semData->define($uri, $this->type);

        if (! is_array($data)) {
            return;
        }

        foreach ($data as $field => $value) {
            if ($this->shouldAddField($field, $value)) {
                $this->processField($uri, $field, $value);
            }
        }
    }

    protected function processConfig($line)
    {
        if (isset($this->config["separator"])) {
            // TODO unescape $this->config["separator"];
            $separator = "\x09";
        } else {
            $separator = " ";
        }

        $configArray = explode($separator, $line);

        if (count($configArray) > 2) {
            $configKey = array_shift($configArray);
            $this->config[$configKey] = $configArray;
        } elseif (count($configArray) > 1) {
            $this->config[$configArray[0]] = $configArray[1]; 
        }
    }

    protected function processField($uri, $field, $value)
    {
        $belongsToArray = $this->belongsTo($uri, $field);
        if (count($belongsToArray) == 2) {
            $finalUri = $belongsToArray[0];
            $finalRelation = $belongsToArray[1];
        } else {
            $finalUri = $uri;
            $finalRelation = $this->getRelationName($field);
        }

        if ($this->shouldExportField($field)) {
            $finalValue = $this->exportField($field, $value);
        } else {
            $finalValue = $this->handleValue($field, $value);
        }

        $this->semData->add($finalUri, $finalRelation, $finalValue);
    }

    private function identifyFileFormat($line)
    {
        if (strlen($line) > 0 && $line[0] === '{') {
            $this->file_format = "JSON";
        }
    }

    private function shouldAddField($field, $value)
    {
        if ($this->file_format === 'JSON') {
            return array_key_exists($field, $this->fields);
        }

        return array_key_exists($field, $this->fields) &&
            $value != $this->config["empty_field"] &&
            $value != $this->config["unset_field"];
    }

    private function getURI($uid, $type = null)
    {
        $domain = "cimic-coe.org";
        $type = strtolower(is_null($type) ? $this->type : $type);
        $uid = str_replace(['.'], ['-'], $uid);
        return "http://{$domain}/{$type}/{$uid}";
    }

    private function handleValue($field, $value)
    {
        if (array_key_exists("handler", $this->fields[$field])) {
            $handler = "handle" . ucfirst($this->fields[$field]["handler"]);

            if (method_exists($this, $handler)) {
                return call_user_func([$this, $handler], $value);
            }
        }
        return $value;
    }

    private function getRelationName($field)
    {
        if (array_key_exists("relation", $this->fields[$field])) {
            return $this->fields[$field]["relation"];
        } else {
            return $field;
        }
    }

    private function shouldExportField($field)
    {
        return array_key_exists("export", $this->fields[$field]);
    }

    private function exportField($field, $value)
    {
        $exportArray = explode(":", $this->fields[$field]["export"]);
        $exportType = $exportArray[0];
        $exportUri = $this->getURI($value, $exportType);
        $this->semData->define($exportUri, $exportType);

        if (count($exportArray) > 1) {
            $this->semData->add(
                $exportUri,
                $exportArray[1],
                $this->handleValue($field, $value)
            );
        }

        return $exportUri;
    }

    private function belongsTo($uri, $field)
    {
        if (!array_key_exists("belongsTo", $this->fields[$field])) {
            return [];
        }

        $belongsToInput = explode(":", $this->fields[$field]["belongsTo"]);
        $belongsToEntity = $this->semData->getFirst($uri, $belongsToInput[0]);

        if (is_null($belongsToEntity)) {
            return [];
        }

        return [
            $belongsToEntity,
            count($belongsToInput) > 1 ? 
                $belongsToInput[1] : 
                $this->getRelationName($field)
        ];
    }

    public function handleBoolean($value)
    {
        return $value == 'T';
    }
}