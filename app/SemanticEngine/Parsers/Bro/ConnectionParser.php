<?php

namespace App\SemanticEngine\Parsers\Bro;

class ConnectionParser extends BroParser
{
    protected $uidKey = 'uid';
    protected $type = "Connection";
    protected $fields = [
        "ts" => [ "relation" => "startedAt" ],
        "id.orig_h" => [ "relation" => "originatingIp", "export" => "IpAddress:ipv4" ],
        "id.orig_p" => [ "relation" => "originatingPort" ],
        "id.resp_h" => [ "relation" => "respondingIp", "export" => "IpAddress:ipv4" ],
        "id.resp_p" => [ "relation" => "respondingPort" ],
        "proto" => [ "relation" => "protocol" ],
        "service" => [],
        "duration" => [],
        "orig_bytes" => [ "relation" => "originatingBytes" ],
        "resp_bytes" => [ "relation" => "respondingBytes" ],
        "conn_state" => [ "relation" => "status" ],
        "local_orig" => [ "belongsTo" => "originatingIp:isLocal", "handler" => "boolean" ],
        "local_resp" => [ "belongsTo" => "respondingIp:isLocal", "handler" => "boolean" ],
    ];
}