<?php

namespace App\SemanticEngine\Parsers;

use Log;
use App\SemanticEngine\Data\SemanticData;

abstract class Parser
{
    public function parse($rawData, $dataType, $semData) {
        $functionName = "parse" . studly_case($dataType);
        if (method_exists($this, $functionName)) {
            return call_user_func_array(
                [$this, $functionName], 
                [$rawData, $semData]
            );
        } else {
            self::logDataTypeNotImplemented($dataType, get_class($this));
            return $semData;
        }
    }

    private static function logDataTypeNotImplemented($dataType, $class) {
        Log::warning("Data type '$dataType' not implemented in '$class'");
    }
}