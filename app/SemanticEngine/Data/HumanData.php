<?php

namespace App\SemanticEngine\Data;

use App\SemanticEngine\Data\Node;

class HumanData
{
    protected $root = null;
    protected $nodeMap;
    protected $suggestions = null;

    function __construct($root = null)
    {
        $this->nodeMap = collect([]);
        if ($root) {
            $this->setRootNode($root);
        }
    }

    public function setRootNode(Node $node)
    {
        $this->addNode($node);
        $this->root = $node;
    }

    public function addNode($node)
    {
        if ($this->hasNode($node)) {
            return;
        }

        if (is_string($node)) {
            $this->nodeMap->put($node, new Node($node));
        }

        if ($node instanceof Node) {
            $this->nodeMap->put($node->getUid(), $node);
        }
    }

    public function addTo($node, $relation, $rawValue)
    {
        // Retrieve source node or do nothing.
        $node = $this->getNode($node);
        if (! $node) {
            return;
        }

        // Get destination node if it is one.
        if ($this->isNode($rawValue)) {
            $value = $this->getNode($rawValue);
        } else {
            $value = $rawValue;
        }

        $node->add($relation, $value);
    }

    public function getRootType()
    {
        if (! $this->root) {
            return null;
        }
        
        return $this->root->getType();
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function getNode($node)
    {
        if (is_string($node)) {
            return $this->nodeMap->get($node);
        }

        if ($node instanceof Node) {
            return $this->nodeMap->get($node->getUid());
        }
    }

    public function getNodes()
    {
        return $this->nodeMap;
    }

    public function getNodesFromLeaves()
    {
        if (! $this->root) {
            return collect([]);
        }

        return $this->root->getNodesFromLeaves();
    }

    public function addSuggestions($suggestions)
    {
        $this->suggestions = collect($suggestions);
    }

    public function getSuggestions()
    {
        return $this->suggestions;
    }

    public function hasSuggestions()
    {
        return is_object($this->suggestions) && ! $this->suggestions->isEmpty();
    }

    public function hasNode($node)
    {
        if (is_string($node)) {
            return $this->nodeMap->has($node);
        }

        if ($node instanceof Node) {
            return $this->nodeMap->has($node->getUid());
        }

        return false;
    }

    public function isNode($node)
    {
        if (is_string($node)) {
            return $this->nodeMap->has($node);
        }

        return $node instanceof Node;
    }

    public function toJson()
    {
        if (is_null($this->root)) {
            return collect([])->toJson();
        }
        return $this->root->getContent()->toJson();
    }

    public function __toString()
    {
        if (is_null($this->root)) {
            return "No root node";
        }
        return $this->root->__toString();
    }
}