<?php

namespace App\SemanticEngine\Data;

class SemanticData
{
    protected $data;

    function __construct($data = null)
    {
        $this->data = is_null($data) ? collect([]) : $data;
    }

    public function define($src, $type)
    {
        return $this->add($src, Data::TYPE_RELATION, $type);
    }

    public function add($src, $relation, $dest)
    {
        $this->data->push([$src, $relation, $dest]);
        return $this;
    }

    public function getType($src)
    {
        return $this->getFirst($src, Data::TYPE_RELATION);
    }

    public function getFirst($src, $relation = null)
    {
        return $this->get($src, $relation, true);
    }

    public function get($src, $relation = null, $first = false)
    {
        return $this->getFromLeftOrRight($src, $relation, $first);
    }

    public function getReversed($dest, $relation = null, $first = false)
    {
        return $this->getFromLeftOrRight($dest, $relation, $first, 2);
    }

    public function isEmpty()
    {
        return $this->data->isEmpty();
    }

    public function hasSource($src)
    {
        return $this->data->contains(function ($value) use($src) {
            return $value[0] === $src;
        });
    }

    public function nodesWhereDestinationMatches($dest)
    {
        return $this->data
            ->filter(function($item) use ($dest) {
                return str_contains(strtolower($item[2]), explode(' ', strtolower($dest)));
            })
            ->pluck(0)
            ->unique()
            ->toArray();
    }

    public function __toString()
    {
        return $this->data->unique()->map(function($item) {
            return implode("\t", $item);
        })->implode("\n");
    }

    private function getFromLeftOrRight(
        $primaryValue, 
        $relation = null,
        $first = false,
        $primaryIndex = 0)
    {
        $ret = is_null($relation) ? 
            $this->data : 
            $this->data->where(1, $relation);

        $ret = $ret
            ->whereStrict($primaryIndex, $primaryValue)
            ->unique()
            ->groupBy(1)
            ->map(function($items) use($primaryIndex) { 
                return $items->map(function($item) use($primaryIndex) {
                    return $item[abs(2-$primaryIndex)];
                });
            });

        if ($first) {
            return $ret->flatten()->first();
        }

        return $ret->toArray();
    }
}