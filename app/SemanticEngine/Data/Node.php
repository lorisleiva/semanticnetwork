<?php

namespace App\SemanticEngine\Data;

class Node
{
    protected $uid;
    protected $content;
    protected $type = null;
    protected $parents;
    protected $children;
    protected $timestamp = 0;
    
    function __construct($uid)
    {
        $this->uid = $uid;
        $this->content = collect([]);
        $this->parents = collect([]);
        $this->children = collect([]);
        $this->add(Data::UID_RELATION, $this->uid);
    }

    /*
     * CHECKERS
     */

    public function has($relation)
    {
        return $this->content->has($relation);
    }

    public function hasNode($relation)
    {
        $nodes = $this->get($relation);

        if (! $nodes || empty($nodes)) {
            return false;
        }

        return $nodes[0] instanceof Node;
    }

    public function hasType($type)
    {
        return ! is_null($this->type) && $this->type === $type;
    }

    /*
     * SETTERS
     */

    public function add($relation, $value)
    {
        // Add dependencies or do nothing if the value is a node.
        if ($value instanceof Node && ! $this->addChild($value)) {
            return;
        }

        // Update the node's type if the relation is @type.
        if ($relation === Data::TYPE_RELATION) {
            return $this->addType($value);
        }

        // Accumulate values with the same relation
        if ($this->has($relation)) {
            $currentValue = $this->get($relation);
            if (is_array($currentValue)) {
                $currentValue[] = $value;
                $value = $currentValue;
            } else {
                $value = [ $currentValue, $value ];
            }
        }

        // Nodes relationships should always be in arrays
        elseif ($value instanceof Node) {
            $value = [ $value ];
        }

        // Insert new value
        $this->content->put($relation, $value);
    }

    public function addType($type)
    {
        $this->type = $type;
        $this->content->put(Data::TYPE_RELATION, $type);
    }

    public function addParent(Node $parentNode)
    {
        if ($this->parents->contains($parentNode)) {
            return true;
        }

        return $parentNode->addChild($this);
    }

    public function addChild(Node $childNode)
    {
        if ($this->children->contains($childNode)) {
            return true;
        }

        if ($this->isValidChild($childNode)) {
            $this->children->push($childNode);
            $childNode->parents->push($this);
            return true;
        }

        return false;
    }

    public function remove($relation)
    {
        $this->content->forget($relation);
    }

    /*
     * GETTERS
     */
    
    public function get($relation)
    {
        return $this->content->get($relation);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getExtendedContent()
    {
        return $this->content->map(function($item, $key) {
            if (is_array($item)) {
                $arr = [];
                foreach ($item as $i) {
                    if ($i instanceof Node) {
                        $arr[] = $i->getExtendedContent();
                    } else {
                        $arr[] = $i;
                    }
                }
                return $arr;
            } elseif ($item instanceof Node) {
                return $item->getExtendedContent();
            } else {
                return $item;
            }
        });
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getNodesFromLeaves()
    {
        $nodes = collect([]);
        foreach ($this->children as $child) {
            $nodes = $nodes->merge($child->getNodesFromLeaves());
        }
        return $nodes->push($this)->unique();
    }

    public function __toString()
    {
        return json_encode($this->getExtendedContent(), JSON_PRETTY_PRINT);
    }

    /*
     * HELPERS
     */

    private function isValidChild($childNode)
    {
        if ($this->parents->contains($childNode)) {
            return false;
        }

        foreach ($this->parents as $parentNode) {
            if (! $parentNode->isValidChild($childNode)) {
                return false;
            }
        }

        return true;
    }
}