<?php

namespace App\SemanticEngine\Data;

abstract class Data
{
    const AS_RELATION = "@as";
    const TYPE_RELATION = "@type";
    const UID_RELATION = "@uid";
}