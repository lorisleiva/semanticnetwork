<?php

namespace App\SemanticEngine\Data;

class RawData
{
    protected $data;

    function __construct()
    {
        $this->data = collect([]);
    }

    public function toArray()
    {
        return $this->data->toArray();
    }

    public function add($type, $data, $parserPath)
    {
        $this->data->push([
            'data' => $data,
            'dataType' => $type,
            'parser' => $parserPath,
        ]);
    }

    public function addFile($data, $parserPath)
    {
        $this->add('file', $data, $parserPath);
    }

    public function isEmpty()
    {
        return $this->data->isEmpty();
    }
}