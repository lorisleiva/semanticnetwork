<?php

namespace App\SemanticEngine;

use Illuminate\Support\Collection;

trait GadgetNotation
{
    public function _getGadgets($gadgetsType)
    {
        $gadgetsProperty = camel_case($gadgetsType);
        $generateGadgets = "generate$gadgetsType";
        
        if (property_exists($this, $gadgetsProperty) &&
            ! is_null($this->{$gadgetsProperty})) {
            return $this->{$gadgetsProperty};
        }
        
        return $this->$generateGadgets();
    }

    public function _generateGadgets($gadgetsType)
    {
        // Define name of functions according to gadget in case of overrides.
        $getDefaultGadgets = "getDefault$gadgetsType";
        $parseGadgets = "parse$gadgetsType";
        $instanciateGadgets = "instanciate$gadgetsType";

        // Gadgets to start with.
        if (property_exists($this, "only")) {
            $gadgets = $this->only;
        } else {
            $gadgets = $this->$getDefaultGadgets();
        }
        $gadgets = $this->$parseGadgets($gadgets);

        // Gadgets to include.
        if (property_exists($this, "include")) {
            $gadgets = $gadgets
                ->merge($this->$parseGadgets($this->include))
                ->unique()
                ->flatten();
        }

        // Gadgets to exclude.
        if (property_exists($this, "exclude")) {
            $gadgets = $gadgets
                ->diff($this->$parseGadgets($this->exclude))
                ->flatten();
        }

        // Instanciate the gadgets.
        $instances = $this->$instanciateGadgets($gadgets);
        if (property_exists($this, camel_case($gadgetsType))) {
            $this->{camel_case($gadgetsType)} = $instances;
        }
        return $instances;
    }

    public function _getDefaultGadgets($gadgetsType)
    {
        return collect([ "Generic:all", "all" ]);
    }

    public function _parseGadgets($gadgetsType, $rawGadgets)
    {
        $parsedGadgets = collect([]);

        foreach ($rawGadgets as $rawGadget) {
            $arr = explode(":", $rawGadget);

            if (count($arr) >= 2) {
                $folder = $arr[0];
                $class = $arr[1];
            } 
            elseif (count($arr) == 1 && !is_null($this->gadgetDefaultFolder)) {
                $folder = $this->gadgetDefaultFolder;
                $class = $arr[0];
            } 
            else {
                // We cowardly abandon the unparsable gadget.
                continue;
            }

            if ($class === "all") {
                $getGadgetsFromFolder = "get${gadgetsType}FromFolder";
                $parsedGadgets = $parsedGadgets
                    ->merge($this->$getGadgetsFromFolder($folder));
            } else {
                $parsedGadgets->push("$folder:$class");
            }
        }

        return $parsedGadgets->unique()->flatten();
    }

    public function _getGadgetsFromFolder($gadgetsType, $folderName)
    {
        $inferenceClasses = collect([]);
        $gadgetType = str_singular($gadgetsType);

        $folder = base_path("$this->gadgetPath/$folderName");
        foreach(glob("$folder/*$gadgetType.php") as $filename){


            $className = class_basename($filename);
            $className = explode(".", $className)[0];
            $inferenceClasses->push("$folderName:$className");
        }

        return $inferenceClasses;
    }

    public function _instanciateGadgets($gadgetsType, $parsedGadgets)
    {
        $gadgets = collect([]);

        foreach ($parsedGadgets as $parsedGadget) {
            $arr = explode(":", $parsedGadget);

            // At this point the gadgets should be parsed.
            if (count($arr) < 2) {
                continue;
            }

            $className = "$this->gadgetPath/$arr[0]/$arr[1]";
            $className = str_replace('/', '\\', $className);
            $gadgets->push(new $className);
        }

        return $gadgets;
    }

    public function __call($name, $args)
    {
        $magicFunctions = [
            ["/^parse([A-Z]\w*)/", "_parseGadgets", 1],
            ["/^get([A-Z]\w*)FromFolder$/", "_getGadgetsFromFolder", 1],
            ["/^getDefault([A-Z]\w*)/", "_getDefaultGadgets", 0],
            ["/^get([A-Z]\w*)$/", "_getGadgets", 0],
            ["/^generate([A-Z]\w*)$/", "_generateGadgets", 0],
            ["/^instanciate([A-Z]\w*)/", "_instanciateGadgets", 1],
        ];

        foreach ($magicFunctions as $arr) {
            if (preg_match($arr[0], $name, $matches)) {
                $gadgetType = $matches[1];
                if (count($args) < $arr[2]) {
                    trigger_error(
                        'Missing arguments on method '.__CLASS__.'::'.$name.'()'
                    );
                }
                array_unshift($args, $gadgetType);
                return call_user_func_array([$this, $arr[1]], $args);
            }
        };

        trigger_error(
            'Call to undefined method '.__CLASS__.'::'.$name.'()', E_USER_ERROR
        );
    }
}