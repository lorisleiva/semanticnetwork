<?php

namespace App\SemanticEngine;

use App\SemanticEngine\Data\HumanData;
use App\SemanticEngine\Data\Node;
use App\SemanticEngine\Data\RawData;
use App\SemanticEngine\Data\SemanticData;
use App\SemanticEngine\Factories\ComponentFactory;

class SemanticEngine
{
    protected $factory;
    protected $previousData;

    function __construct(ComponentFactory $factory)
    {
        $this->setComponentFactory($factory);
    }

    public function search($query)
    {
        $this->guessType($query);

        $rawData = $this->fetch($query);
        $semData = $this->parse($rawData);
        $node    = $this->getNode($semData, $query);
        $humData = $this->expandNode($semData, $node);

        $this->addSuggestions($humData, $query);
        $this->attachPreviousData($query, $rawData, $semData);

        $this->infer($humData);
        return $this->getSections($humData);
    }

    public function setComponentFactory(ComponentFactory $factory)
    {
        $this->factory = $factory;
    }

    public function guessType($query)
    {
        $this->factory->createTypeGuesser()->guess($query); 
    }

    public function fetch($query)
    {
        return $this->factory->createFetcher($query)->fetch();
    }

    public function parse(RawData $rawData, SemanticData $semData = null)
    {
        $semData = is_null($semData) ? new SemanticData : $semData;

        foreach ($rawData->toArray() as $rawDataWrapper) {
            $parser = $this->factory->createParser($rawDataWrapper['parser']);
            if ($parser) {
                $parser->parse(
                    $rawDataWrapper['data'], 
                    $rawDataWrapper['dataType'], 
                    $semData
                );
            }
        }
        
        return $semData;
    }

    public function getNode(SemanticData $semData, $query)
    {
        return $this->factory->createNodeGetter($semData, $query)->get();
    }

    public function expandNode(SemanticData $semData, Node $node = null, $depth = null)
    {
        return $this->factory->createNodeExpander($semData, $node)->expand($depth);
    }

    public function infer(HumanData $humData)
    {
        $this->factory->createInferrer($humData)->infer();
    }

    public function getSections(HumanData $humData)
    {
        return $this->factory->createPresenter($humData)->present();
    }

    private function addSuggestions(HumanData $humData, $query)
    {
        if ($query->has('suggestions')) {
            $humData->addSuggestions($query->get('suggestions'));
        }
    }

    private function attachPreviousData($query, $rawData, $semData)
    {
        $this->previousData = compact('query', 'rawData', 'semData');
    }

    public function getPreviousData()
    {
        return $this->previousData;
    }
}