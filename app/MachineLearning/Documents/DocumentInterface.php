<?php

namespace App\MachineLearning\Documents;

interface DocumentInterface
{
    public function getData();
    // TODO: Create TransformationInterface
    // public function applyTransformation($transform);
}
