<?php

namespace App\MachineLearning\Documents;

class RecordDocument implements DocumentInterface 
{
    protected $record;

    function __construct(array $record = null)
    {
        $this->record = is_null($record) ? [] : $record;
    }

    public function put($field, $value)
    {
        $this->record[$field] = $value;
    }

    public function get($field)
    {
        if (array_key_exists($field, $this->record)) {
            return $this->record[$field];
        }
    }

    public function __get($field)
    {
        return $this->get($field);
    }

    public function getData()
    {
        return $this->record;
    }
}