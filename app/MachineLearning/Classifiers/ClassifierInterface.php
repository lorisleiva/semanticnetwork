<?php

namespace App\MachineLearning\Classifiers;

use App\MachineLearning\Documents\DocumentInterface;

interface ClassifierInterface
{
    public function classify(DocumentInterface $document);
}
