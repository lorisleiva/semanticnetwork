<?php

namespace App\MachineLearning\Classifiers;

use App\MachineLearning\Documents\DocumentInterface;
use App\MachineLearning\FeatureFactories\FeatureFactoryInterface;
use App\MachineLearning\Models\ModelInterface;

class LevenshteinClassifier implements ClassifierInterface
{
    protected $model;
    protected $ff;

    function __construct(ModelInterface $model, FeatureFactoryInterface $ff)
    {
        $this->model = $model;
        $this->ff = $ff;
    }

    /**
     * The first feature is used as the string to compare and 
     * the second as the class it belongs.
     * @param  DocumentInterface $document The document to classify.
     * @return array The class identified with its probability.
     */
    public function classify(DocumentInterface $document)
    {
        $str1 = $this->ff->getFeatures($document)[0];
        $scores = $this->model
            ->getDocuments()
            ->map(function($doc) {
                return $this->ff->getFeatures($doc);
            })
            ->groupBy(1)
            ->map(function($tuples) use($str1) {
                $classScore = $tuples
                    ->map(function($tuple) use($str1) {
                        $str2 = $tuple[0];
                        $maxLength = max(strlen($str1), strlen($str2));
                        return $this->levenshtein($str1, $str2) / $maxLength;
                    })->min();
                return 1 - $classScore;
            })
            ->sort();
        
        return $scores;
    }

    private function levenshtein($str1, $str2)
    {
        $l1 = strlen($str1);
        $l2 = strlen($str2);
        $dis = range(0,$l2);

        for($x = 1; $x <= $l1; $x++){     
            $dis_new[0] = $x;
            for($y = 1; $y <= $l2; $y++){
                $dis_new[$y] = min(
                    $dis[$y] + 1,
                    $dis_new[$y-1] + 1,
                    $dis[$y-1] + $this->substitutionCost($str1[$x-1], $str2[$y-1])
                );  
            }
            $dis = $dis_new;              
        }   

        return $dis[$l2];
    }

    private function substitutionCost($char1, $char2)
    {
        if ($char1 == $char2) 
        {
            return 0;
        }
        if (is_numeric($char1) && is_numeric($char2)) 
        {
            return 0.5;
        }
        if ((ctype_upper($char1) && ctype_upper($char2)) ||
            (ctype_lower($char1) && ctype_lower($char2))) 
        {
            return 0.75;
        }
        return 1;
    }
}