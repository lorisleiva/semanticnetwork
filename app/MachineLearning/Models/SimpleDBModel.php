<?php

namespace App\MachineLearning\Models;

use App\MachineLearning\Documents\RecordDocument;
use DB;

class SimpleDBModel extends Model
{
    protected $db_table;

    function __construct($db_table)
    {
        $this->db_table = $db_table;
    }

    public function getDocuments()
    {
        return $this->getAll();
    }

    public function train(RecordDocument $document)
    {
        if ( ! $this->contains($document))
        {
            $this->insert($document);
        }
    }

    public function trainArray(array $trainingSet)
    {
        foreach ($trainingSet as $trainingDocument) {
            $this->train(new RecordDocument($trainingDocument));
        }
    }

    public function getAll()
    {
        return DB::table($this->db_table)->get()
            ->map(function($item) {
                return new RecordDocument((array) $item);
            });
    }

    public function insert(RecordDocument $document)
    {
        return DB::table($this->db_table)
            ->insert($this->getInsertArray($document));
    }

    public function contains(RecordDocument $document)
    {
        return DB::table($this->db_table)
            ->where($this->getWhereArray($document))
            ->exists();
    }

    public function getInsertArray(RecordDocument $document)
    {
        return $document->getData();
    }

    public function getWhereArray(RecordDocument $document)
    {
        $whereArray = [];
        foreach ($document->getData() as $field => $value) {
            $whereArray[] = [$field, $value];
        }
        return $whereArray;
    }
}