<?php

namespace App\MachineLearning\Models;

interface ModelInterface
{
    public function getDocuments();
}
