<?php

namespace App\MachineLearning\FeatureFactories;

use App\MachineLearning\Documents\DocumentInterface;

class FeaturesByAccessors implements FeatureFactoryInterface
{
    protected $accessors;

    function __construct(array $accessors = [])
    {
        $this->accessors = $accessors;
    }

    public function addAccessor($accessor)
    {
        if (! is_null($accessor)) {
            $this->accessors[] = $accessor;
        }
    }

    public function getFeatures(DocumentInterface $document)
    {
        $result = [];
        foreach ($this->accessors as $accessor) {
            $result[] = $this->getValue($accessor, $document);
        }
        return $result;
    }

    private function getValue($accessor, DocumentInterface $document)
    {
        $data = $document->getData();

        if (is_string($accessor) && 
            is_array($data) && 
            array_key_exists($accessor, $data)) 
        {
            return $data[$accessor];
        }

        if (! is_string($accessor) && is_callable($accessor)) {
            return $accessor($document);
        }

        return null;
    }
}
