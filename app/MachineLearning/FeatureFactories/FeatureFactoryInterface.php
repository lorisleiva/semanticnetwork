<?php

namespace App\MachineLearning\FeatureFactories;

use App\MachineLearning\Documents\DocumentInterface;

interface FeatureFactoryInterface
{
    public function getFeatures(DocumentInterface $document);
}
