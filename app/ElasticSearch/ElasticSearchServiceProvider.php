<?php

namespace App\ElasticSearch;

use App;
use App\ElasticSearch\ElasticSearchClient;
use Illuminate\Support\ServiceProvider;

class ElasticSearchServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        App::bind('elasticsearch', function()
        {
            return new ElasticSearchClient;
        });
    }
}

