<?php

namespace App\ElasticSearch;

use Carbon\Carbon;
use Elasticsearch\ClientBuilder;

class ElasticSearchClient
{
    const MAX_RESULT_SIZE = 1000;

    protected $canBeUsed = false;

    protected $es;

    protected $index = 'bro-*';

    protected $params;

    protected $timerange;

    protected $timelessTypes = [ 'IpAddress' ];
    
    function __construct()
    {
        $this->params = $this->getDefaultParams();

        $this->es = ClientBuilder::create()
            ->setHosts(['localhost:9200'])
            ->build();
    }

    public function find($uid, $type = null)
    {
        $search_text = "+_id:$uid" . ($type ? " +_type:$type" : '');
        $this->params = $this->getQueryStringParams($search_text, $type);
        $response = $this->first();

        return $this->polishResponse($response);
    }

    public function get($index, $type, $uid)
    {
        $response =  $this->es->get([
            'index' => $index,
            'type' => $type,
            'id' => $uid
        ]);

        return $this->polishResponse($response);
    }

    public function search($search_text, $type = null)
    {
        $this->params = $this->getQueryStringParams($search_text, $type);

        return $this;
    }

    public function first($size = 1)
    {
        $response = $this->es->search(array_merge($this->params, [
            'size' => $size
        ]));

        return $this->polishResponse($response);
    }

    public function count()
    {
        $response = $this->es->count($this->params);

        return $response['count'];
    }

    public function all()
    {
        return $this->first(self::MAX_RESULT_SIZE);
    }

    public function setTimerange(Carbon $from = null, Carbon $to = null)
    {
        $rangeQuery['format'] = "yyyy-MM-dd'T'HH:mm:ssZ";

        if ($from) {
            $rangeQuery['gte'] = $from->toIso8601String();
        }

        if ($to) {
            $rangeQuery['lte'] = $to->toIso8601String();
        }

        $this->timerange = [ 'range' => [ '@timestamp' => $rangeQuery ] ];
    }

    public function enable()
    {
        $this->canBeUsed = true;
    }

    public function disable()
    {
        $this->canBeUsed = false;
    }

    public function canBeUsed()
    {
        return $this->canBeUsed;
    }

    private function getQueryStringParams($search_text, $type = null)
    {
        $params = [
            'index' => $this->getIndex($type),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'query_string' => [
                                    'query' => $search_text,
                                    'analyze_wildcard' => true
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if ($type) {
            $params['type'] = $type;
        }

        if ($this->queryShouldBeWrappedInTimerange($type)) {
            $params['body']['query']['bool']['must'][] = $this->timerange;
        }

        return $params;
    }

    private function getDefaultParams()
    {
        return [
            'index' => $this->getIndex()
        ];
    }

    private function getIndex($type = null)
    {
        if ($type === 'IpAddress') {
            return 'bro-ipaddress';
        }

        return $this->index;
    }

    private function polishResponse($response)
    {
        if (array_key_exists('hits', $response)) {
            $response = $response['hits']['hits'];
        }

        if (count($response) == 1) {
            $response = $response[0];
        }
        
        return $response;
    }

    private function queryShouldBeWrappedInTimerange($type = null)
    {
        return $this->timerange && ! in_array($type, $this->timelessTypes);
    }
}
