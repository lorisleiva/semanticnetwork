<?php

namespace App\ElasticSearch;

use App\SemanticEngine\Data\Node;

class NodeAdapter extends Node
{
    const RELATIONS_TO_SKIP = [
        'type', '@version', 'tags', '@timestamp', 'uid'
    ];

    function __construct($esResponse)
    {
        parent::__construct($esResponse['_id']);
        $this->addType($esResponse['_type']);
        $this->parseSourceFromElasticSearch($esResponse['_source']);
    }

    private function parseSourceFromElasticSearch($source)
    {
        foreach ($source as $relation => $value) {
            $this->parsePropertyFromElasticSearch($relation, $value);
        }
    }

    private function parsePropertyFromElasticSearch($relation, $value)
    {
        if (in_array($relation, self::RELATIONS_TO_SKIP)) {
            return;
        }

        $this->add($relation, $value);
    }

    public static function generateAll($esData)
    {
        if (array_key_exists('_source', $esData)) {
            return [ new NodeAdapter($esData) ];
        }

        return array_map(function($item) { 
            return new NodeAdapter($item);
        }, $esData);
    }
}