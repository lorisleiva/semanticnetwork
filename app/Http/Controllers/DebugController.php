<?php

namespace App\Http\Controllers;

use App\ElasticSearch\ElasticSearchClient;
use App\ElasticSearch\NodeAdapter;
use App\MachineLearning\Classifiers\LevenshteinClassifier;
use App\MachineLearning\Documents\RecordDocument;
use App\MachineLearning\FeatureFactories\FeaturesByAccessors;
use App\MachineLearning\Models\SimpleDBModel;
use App\SemanticEngine\Factories\ElasticSearchFactory;
use App\SemanticEngine\Parsers\Bro\BroParser;
use App\SemanticEngine\TypeGuessers\ElasticSearchTypeGuesser;
use App\SemanticEngine\TypeGuessers\LevenshteinTypeGuesser;
use App\SemanticEngine\TypeGuessers\RegexTypeGuesser;
use Carbon\Carbon;
use ES;
use Illuminate\Http\Request;
use SemanticEngine;

class DebugController extends Controller
{
    public function semantic_engine()
    {
        $processes = $this->getSemanticEngineProcesses();
        
        return view('debug.processes')->with([
            'title' => 'Semantic Engine (version 1)',
            'processes' => $processes
        ]);
    }

    public function semantic_engine_2()
    {
        SemanticEngine::setComponentFactory(new ElasticSearchFactory);
        $processes = $this->getSemanticEngineProcesses();
        
        return view('debug.processes')->with([
            'title' => 'Semantic Engine (version 2)',
            'processes' => $processes
        ]);
    }

    public function machine_learning()
    {
        $testing = [
            ['IpAddress','10.2.221.32'],
            ['IpAddress','250.2.221.32'],
            ['IpAddress','10.2.233.101'],
            ['Connection','COZblt12vmpssRDQJj']
        ];
        $training = [];

        $model = new SimpleDBModel('ml_typeguesser');
        $model->trainArray($training);
        $ff = new FeaturesByAccessors(['query', 'type']);
        $cls = new LevenshteinClassifier($model, $ff);
        $training = $model->getDocuments();
        $correct = 0;
        $predictions = [];

        foreach ($testing as $d)
        {
            $result = $cls->classify(new RecordDocument([
                'type' => $d[0],
                'query' => $d[1],
            ]));
            $predictions[] = "$d[1] predicted to be $result[0] with certainty " . number_format($result[1]*100,2) . "%";
            if ($result[0] === $d[0])
                $correct ++;
        }

        $predictions[] = "Accuracy: " . number_format(100*$correct / count($testing), 2) . "%";
        $input = [];
        foreach ($training as $trainingDoc) {
            $input[] = "(" . $trainingDoc->get("type") . " => " . $trainingDoc->get("query") . ")";
        }

        $processes = collect([]);
        $processes->push([
            'type' => 'pretty',
            'title' => 'Levenshtein Classifier',
            'input' => $input,
            'output' => 'predictions',
            'data' => $predictions
        ]);

        return view('debug.processes')->with([
            'title' => 'Machine Learning',
            'processes' => $processes
        ]);
    }

    public function type_guess()
    {
        $processes = collect([]);

        $size = 10;
        $query = collect([
            "search" => request('query', 'default'),
            "date_from" => Carbon::createFromFormat('d/m/Y H:i', '01/01/2017 10:00', 'Europe/Amsterdam'),
            "date_to" => null,
            "type" => null,
            "suggestions" => []
        ]);
        ES::setTimerange($query['date_from'], $query['date_to']);

        $processes->push([
            'type' => 'pretty',
            'title' => 'Initial query',
            'output' => 'query',
            'data' => unserialize(serialize($query))
        ]);

        (new RegexTypeGuesser(new LevenshteinTypeGuesser))->guess($query);
        $processes->push([
            'type' => 'suggestions',
            'title' => 'Regex followed by Levenshtein Type Guessers',
            'input' => [ "query" ],
            'output' => 'query',
            'data' => unserialize(serialize($query))
        ]);

        $esData = ES::search($query->get('search'))->first($size);
        $esDataTyped = ES::search($query->get('search'), $query->get('type'))->first($size);
        if (array_key_exists('_score', $esData)) $esData = [ $esData ];
        if (array_key_exists('_score', $esDataTyped)) $esDataTyped = [ $esDataTyped ];

        $processes->push([
            'type' => 'html',
            'title' => 'ElasticSearch call',
            'input' => [ "query: " . $query->get('search'), "size: $size" ],
            'output' => 'elastic search response',
            'data' => $this->getScoreList($esData) . "<hr>" . $this->getScoreList($esDataTyped)
        ]);

        (new ElasticSearchTypeGuesser)->guess($query);
        $processes->push([
            'type' => 'suggestions',
            'title' => 'ElasticSearch Type Guesser',
            'input' => [ "query" ],
            'output' => 'query',
            'data' => unserialize(serialize($query))
        ]);

        return view('debug.processes')->with([
            'title' => 'Type Guessing Process',
            'processes' => $processes
        ]);
    }

    private function getSemanticEngineProcesses()
    {
        $processes = collect([]);

        $query = collect([
            "search" => request('query', '8.8.8.8'),
            "date_from" => Carbon::createFromFormat('d/m/Y H:i', '06/03/2017 06:00', 'Europe/Amsterdam'),
            "date_to" => Carbon::createFromFormat('d/m/Y H:i', '06/03/2017 07:00', 'Europe/Amsterdam'),
            "type" => null
        ]);
        $processes->push([
            'type' => 'pretty',
            'title' => 'Input',
            'output' => 'query',
            'data' => unserialize(serialize($query))
        ]);

        ES::setTimerange($query['date_from'], $query['date_to']);
        SemanticEngine::guessType($query);
        $processes->push([
            'type' => 'pretty',
            'title' => 'Type Guessers',
            'input' => ['query'],
            'output' => 'query',
            'data' => unserialize(serialize($query))
        ]);

        $rawData = SemanticEngine::fetch($query);
        $processes->push([
            'type' => 'pretty',
            'title' => 'Fetchers',
            'input' => ['query'],
            'output' => 'rawData',
            'data' => $rawData->toArray()
        ]);

        $semData = SemanticEngine::parse($rawData);
        $processes->push([
            'type' => 'pre',
            'title' => 'Parsers',
            'input' => ['rawData'],
            'output' => 'semData',
            'data' => (substr($semData, 0, 1000)) . " [...]"
        ]);

        $node = SemanticEngine::getNode($semData, $query);
        $processes->push([
            'type' => 'pre',
            'title' => 'Node Getters',
            'input' => ['semData', 'query'],
            'output' => 'node',
            'data' => unserialize(serialize($node))
        ]);

        $humData = SemanticEngine::expandNode($semData, $node);
        $processes->push([
            'type' => 'pre',
            'title' => 'Node Expanders',
            'input' => ['semData', 'node'],
            'output' => 'humData',
            'data' => unserialize(serialize($humData))
        ]);

        SemanticEngine::infer($humData);
        $processes->push([
            'type' => 'pre',
            'title' => 'Inferences',
            'input' => ['humData'],
            'output' => 'humData',
            'data' => $humData
        ]);

        $sections = SemanticEngine::getSections($humData);
        $processes->push([
            'type' => 'pretty',
            'title' => 'Sections',
            'input' => ['humData'],
            'output' => 'sections',
            'data' => $sections
        ]);

        return $processes;
    }

    private function getScoreList($esData) {
        return implode('', array_map(function($item) {
            return "<p><b>" . $item["_score"] . " </b> " . $item["_id"] . " <span class='tag is-dark'>" . $item["_type"] . "</span></p>";
        }, $esData));
    }
}
