<?php

namespace App\Http\Controllers;

use App\Http\Requests\QueryRequest;
use App\MachineLearning\Classifiers\LevenshteinClassifier;
use App\MachineLearning\Documents\RecordDocument;
use App\MachineLearning\FeatureFactories\FeaturesByAccessors;
use App\MachineLearning\Models\SimpleDBModel;
use DB;
use Response;
use SemanticEngine;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function search(QueryRequest $request)
    {
        $request->updateComponentFactory();
        $request->updateTimeRange();
        $sections = SemanticEngine::search($request->toQuery());

        return $this->respond($sections);
    }

    public function train()
    {
        $model = new SimpleDBModel('ml_typeguesser');
        $model->train(new RecordDocument([
            'query' => request('query'),
            'type' => request('type')
        ]));

        return $this->respond([]);
    }

    public function types()
    {
        $types = array_merge(
            scandir_for_types('Fetchers'),
            scandir_for_types('Inferences'),
            scandir_for_types('Sections'),
            DB::table('ml_typeguesser')
                ->distinct()->select('type')->get()
                ->pluck('type')->toArray()
        );

        return array_unique($types);
    }

    public function trainingData()
    {
        return DB::table('ml_typeguesser')->get();
    }

    public function deleteTrainingData()
    {
        return DB::table('ml_typeguesser')->where([
            'query' => request('query'),
            'type' => request('type')
        ])->delete();
    }

    public function classify()
    {
        $model = new SimpleDBModel('ml_typeguesser');
        $ff = new FeaturesByAccessors(['query', 'type']);
        $cls = new LevenshteinClassifier($model, $ff);
        return $cls->classify(new RecordDocument([
            'query' => request('query')
        ]));
    }

    /*
    |--------------------------------------------------------------------------
    | API HELPERS
    |--------------------------------------------------------------------------
    */

    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    protected function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }
}
