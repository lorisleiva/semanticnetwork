<?php

namespace App\Http\Requests;

use ES;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use SemanticEngine;

class QueryRequest extends FormRequest
{
    protected $dates;

    public function authorize() { return true; }
    public function rules() { return []; }

    public function updateComponentFactory()
    {
        $factory = $this->input('factory', 'ElasticSearchFactory');

        if ($factory === "ElasticSearchFactory") {
            ES::enable();
        }

        $factory = "App\SemanticEngine\Factories\\$factory";
        SemanticEngine::setComponentFactory(new $factory);
    }

    public function updateTimeRange()
    {
        $dates = $this->getDates();
        ES::setTimerange($dates[0], $dates[1]);
    }

    public function toQuery()
    {
        $dates = $this->getDates();

        return collect([
            "search" => $this->input('query_string'),
            "date_from" => $dates[0],
            "date_to" => $dates[1],
            "type" => $this->input('type'),
            "suggestions" => []
        ]);
    }

    private function getDates()
    {
        return $this->dates ?: $this->parseDates();
    }

    private function parseDates()
    {
        $dates = [ null, null ];

        switch ($this->date_option) {
            case 'week':
                $dates[0] = Carbon::now('Europe/Amsterdam')->subWeek();
                break;

            case 'custom':
                $dates[0] = $this->parseToCarbonDate($this->date_from);
                $dates[1] = $this->parseToCarbonDate($this->date_to);
                break;

            case 'today':
            default:
                $dates[0] = Carbon::now('Europe/Amsterdam')->subDay();
                break;
        }

        $this->dates = $dates;
        return $dates;
    }

    private function parseToCarbonDate($date)
    {
        if (! $date) {
            return;
        }

        return Carbon::createFromFormat('Y-m-d H:i', $date, 'Europe/Amsterdam');
    }
}
