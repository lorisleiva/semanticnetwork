<?php

// Statistics

if (! function_exists('average')) {
    function average($values)
    {
        if (! is_array($values) || empty($values)) {
            return 0;
        }

        return array_sum($values) / count($values);
    }    
}

if (! function_exists('variance')) {
    function variance($values, $sample = false)
    {
        if (! is_array($values) || empty($values)) {
            return 0;
        }
        
        $mean = average($values);
        $variance = 0.0;
        foreach ($values as $i) {
            $variance += pow($i - $mean, 2);
        }
        $variance /= ( $sample ? count($values) - 1 : count($values) );

        return $variance;
    }   
}


// Folders

if (! function_exists('scandir_for_types')) {
    function scandir_for_types($folder)
    {
        $content = scandir(app_path("SemanticEngine/$folder"));
        return array_filter($content, function($element) {
            return ! str_contains($element, '.')
                && $element !== 'Generic'
                && $element !== 'Support';
        });
    }
}