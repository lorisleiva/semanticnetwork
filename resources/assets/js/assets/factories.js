export default [

    {
        name: 'Elastic Search Factory',
        value: 'ElasticSearchFactory',
        icon: 'database'
    },

    {
        name: 'Full Engine Factory',
        value: 'FullEngineFactory',
        icon: 'cogs'
    }

]