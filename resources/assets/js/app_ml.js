import Vue from 'vue';
import App from './layout_ml/App.vue'

new Vue({
    el: '#app',
    render: h => h(App)
});
