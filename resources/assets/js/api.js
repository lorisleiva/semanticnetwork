import axios from 'axios'

axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};


export default {
    search(query) {
        return axios.get('/api/search', { params: query })
    },

    train(query_string, type) {
        return axios.post('/api/train', { query: query_string, type })
    },

    getAvailableTypes() {
        return axios.get('/api/types')
    },

    getTrainingData() {
        return axios.get('/api/training_data')
    },

    deleteTrainingTuple(query, type) {
        return axios.delete('api/training_data', { params : { query, type }})
    },

    classifyTrainingTuple(query) {
        return axios.get('api/classify', { params : { query }})
    }
}
