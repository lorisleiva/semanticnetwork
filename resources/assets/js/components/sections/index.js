import SectionError from './Error'
import SectionSearching from './Searching'
import SectionNoResults from './NoResults'
import SectionTitle from './Title'
import SectionSuggestions from './Suggestions'
import SectionTable from './Table'
import SectionRawHtml from './RawHtml'
import SectionTimeline from './Timeline'

export default {
    SectionError,
    SectionSearching,
    SectionNoResults,
    SectionTitle,
    SectionSuggestions,
    SectionTable,
    SectionRawHtml,
    SectionTimeline,
}