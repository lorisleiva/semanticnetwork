import Vue from 'vue';
import App from './layout/App.vue'
import store from './store'
import VueFlatpickr from 'vue-flatpickr'

Vue.use(VueFlatpickr)

new Vue({
    el: '#app',
    store,
    render: h => h(App)
});
