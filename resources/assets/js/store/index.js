import Vue from 'vue'
import Vuex from 'vuex'
import sections from './sections'
import query from './query'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: { sections, query },
    strict: debug
})