import api from '../api'
import { orderBy } from 'lodash'

export default {

    state: {
        all: [],
        success: true,
        searching: false
    },

    getters: {
        sectionsToDisplay(state) {
            if (state.searching) return [{ type: 'searching' }]
            if (! state.success) return [{ type: 'error' }]
            if (state.all.length < 1) return [{ type: 'no-results' }]
            return state.all
        }
    },

    actions: {
        search({ commit, rootGetters }) {
            commit('searchRequest')
            api .search(rootGetters.query)
                .then(({data}) => commit('searchSuccess', data))
                .catch(() => commit('searchFailure'))
        }
    },

    mutations: {
        searchRequest(state) {
            state.all = []
            state.success = true
            state.searching = true
        },

        searchSuccess(state, sections) {
            state.all = orderBy(sections, 'priority', 'desc')
            state.success = true
            state.searching = false
        },

        searchFailure(state) {
            state.success = false
            state.searching = false
        }
    }
}