import api from '../api'
import { orderBy } from 'lodash'

export default {

    state: {
        query_string: '',
        date_option: 'today',
        date_from: null,
        date_to: null,
        type: '',
        factory: localStorage.getItem('component_factory') || 'FullEngineFactory'
    },

    getters: {
        query(state) {
            return state
        }
    },

    actions: {
        train({ state }, type) {
            api.train(state.query_string, type)
        }
    },

    mutations: {
        updateQueryString(state, query_string) {
            state.query_string = query_string
        },
        updateDateOption(state, date_option) {
            state.date_option = date_option
        },
        updateDateFrom(state, date_from) {
            state.date_from = date_from
        },
        updateDateTo(state, date_to) {
            state.date_to = date_to
        },
        updateType(state, type) {
            state.type = type
        },
        updateFactory(state, factory) {
            state.factory = factory
            localStorage.setItem('component_factory', factory)
        }
    }
}