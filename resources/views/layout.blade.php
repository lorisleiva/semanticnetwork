<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Network Management</title>
    </head>
    <body>
        <div id="app"></div>
        <script>
            window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>
        @yield('javascript')
    </body>
</html>
