@extends('debug.layout')

@section('content')
    <div class="hero is-primary">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    {{ $title }}
                </h1>
            </div>
        </div>
    </div>

    @foreach ($processes as $process)
        <div class="section">
            <div class="container">
                <div class="heading">
                    <h1 class="title">{{ $process['title'] }}</h1>
                </div>

                <hr>
                
                <div class="content">
                    @if (isset($process['input']))
                        <p><u>input:</u></p>
                        <ul>
                            @foreach ($process['input'] as $input)
                                <li>{{ $input }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <p><u>output:</u> {{ $process['output'] }}</p>

                    @if ($process['type'] == 'pre')
                        <blockquote><pre>{{ $process['data'] }}</pre></blockquote>
                    @elseif ($process['type'] == 'pretty')
                        <blockquote><pre>{{ json_encode($process['data'], JSON_PRETTY_PRINT) }}</pre></blockquote>
                    @elseif ($process['type'] == 'dump')
                        <blockquote><pre>{{ var_dump($process['data']) }}</pre></blockquote>
                    @elseif ($process['type'] == 'suggestions')
                        <blockquote>
                            @if ($process['data']['type'])
                                Identified type: <span class="tag is-dark">{{ $process['data']['type'] }}</span>
                            @else
                                No type identified
                            @endif
                        </blockquote>

                        <blockquote>
                            @if (count($process['data']['suggestions']) >= 1)
                                <p>Suggestions identified: </p>
                                <div class="columns">
                                    @foreach ($process['data']['suggestions'] as $suggestion)
                                        <div class="column has-text-centered">
                                            @if ($suggestion['title'])
                                                <p>{{ $suggestion['title'] }}</p>
                                            @endif
                                            <div class="tag is-dark">{{ $suggestion['type'] }}</div>
                                            <p>{{ $suggestion['probability'] }}</p>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                No suggestions identified
                            @endif
                        </blockquote>
                    @else
                        <blockquote>{!! $process['data'] !!}</blockquote>
                    @endif
                </div>

            </div>
        </div>
    @endforeach
@stop