<?php

Route::get('/', 'MainController@home');
Route::get('/machinelearning', 'MainController@machine_learning');

Route::get('debug/v1', 'DebugController@semantic_engine');
Route::get('debug/v2', 'DebugController@semantic_engine_2');
Route::get('debug/ml', 'DebugController@machine_learning');
Route::get('debug/typeguess', 'DebugController@type_guess');
