<?php

Route::get('/search', 'ApiController@search');
Route::post('/train', 'ApiController@train');
Route::get('/types', 'ApiController@types');

Route::get('/training_data', 'ApiController@trainingData');
Route::delete('/training_data', 'ApiController@deleteTrainingData');
Route::get('/classify', 'ApiController@classify');
